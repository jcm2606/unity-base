/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out mat3x4 skySH;

    out vec3 lightColour;
    out vec3 skyColour;

    out vec2 screenCoord;

    // IN
    // UNIFORM
    uniform sampler2D noisetex;

    uniform mat4 gbufferModelViewInverse;

    uniform vec3 sunDirection;

    uniform float wetness;
    uniform float eyeAltitude;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/unity/shared/PhysicalSky.glsl"
    #include "/unity/vertex/PhysicalSkySH.vsh"

    // FUNC
    // MAIN

    void main() {
        gl_Position = ftransform();

        screenCoord = gl_Vertex.xy;

        lightColour = (GetSunColorZom(sunDirection) + GetMoonColorZom(-sunDirection));

        vec3 viewAbsorb = vec3(1.0);
        skyColour = sky_atmosphere(vec3(0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 1.0, 0.0), sunDirection, -sunDirection, skySunColor, skyMoonColor, 10, viewAbsorb);

        CalculateSkySH(viewAbsorb, skyColour);
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    const bool generateShadowMipmap = true;

    // OUT
    /* DRAWBUFFERS:456 */
    layout(location = 0) out vec4 fragColour;
    layout(location = 1) out vec4 shadowPassthrough;
    layout(location = 2) out vec4 skyDome;

    // IN
    in mat3x4 skySH;

    in vec3 lightColour;

    in vec2 screenCoord;
    
    // UNIFORM
    uniform sampler2D colortex0;
    uniform sampler2D colortex1;
    uniform sampler2D colortex2;

    uniform sampler2D depthtex0;

    uniform sampler2D shadowtex0;
    uniform sampler2D shadowtex1;
    uniform sampler2D shadowcolor0;
    uniform sampler2D shadowcolor1;

    uniform sampler2D noisetex;

    uniform mat4 gbufferProjection, gbufferProjectionInverse;
    uniform mat4 gbufferModelView, gbufferModelViewInverse;
    uniform mat4 shadowProjection;
    uniform mat4 shadowModelView;

    uniform vec3 sunDirection;
    uniform vec3 shadowLightDirection;

    uniform vec2 taaJitter;

    uniform float near, far;
    uniform float frameTimeCounter;
    uniform float sunAngle;
    uniform float wetness;
    uniform float eyeAltitude;

    uniform int frameCounter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/InternalSettings.glsl"
    
    #include "/local/shared/Material.glsl"
    #include "/unity/shared/Position.glsl"
    #include "/local/fragment/DiffuseLighting.fsh"
    #include "/unity/shared/PhysicalSky.glsl"

    // FUNC
    vec3 CalculateSkyDome(vec2 screenCoord, vec2 dither) {
        if(any(greaterThan(screenCoord, vec2(skyDomeQuality + 0.01)))) return vec3(0.0);

        vec3 sphericalProjection = ProjectSphere(screenCoord * rSkyDomeQuality) * vec3(1.0, 1.0, -1.0);
        //vec3 sphericalProjectionView = mat3(gbufferModelView) * sphericalProjection;
        //vec3 sphericalViewVector = -sphericalProjectionView;
        
        vec3 viewAbsorb = vec3(0.0);
        vec3 skyDome  = sky_atmosphere(vec3(0.0), normalize(sphericalProjection), vec3(0.0, 1.0, 0.0), sunDirection, -sunDirection, skySunColor, skyMoonColor, 10, viewAbsorb);

        return skyDome;
    }

    // MAIN

    void main() {
        vec2 dither = vec2(Bayer8(gl_FragCoord.xy), 64.0);
    #ifdef TAA
        dither.x = fract(dither.x + LinearBayer16(frameCounter));
    #endif

        skyDome = EncodeRGBE8(CalculateSkyDome(screenCoord, dither));

        Position position = GetPosition(screenCoord, depthtex0);
        if(!isLand(position.depth)) return;

        Material material = GetMaterial(screenCoord);

        vec3 image = CalculateDiffuseLighting(material, position.scenePosition, dither, shadowPassthrough.rgb);

        fragColour = EncodeRGBE8(image);
    }

#endif
