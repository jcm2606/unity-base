/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out vec2 screenCoord;

    // IN
    // UNIFORM
    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    // FUNC
    // MAIN

    void main() {
        gl_Position = ftransform();

        screenCoord = gl_Vertex.xy;
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    const bool colortex3MipmapEnabled = true;

    // OUT
    /* DRAWBUFFERS:4 */
    layout(location = 0) out vec4 fragColour;

    // IN
    in vec2 screenCoord;
    
    // UNIFORM
    uniform sampler2D colortex0;
    uniform sampler2D colortex3;
    uniform sampler2D colortex5;

    uniform sampler2D depthtex0;
    uniform sampler2D depthtex1;
    uniform sampler2D depthtex2;

    uniform mat4 gbufferProjection, gbufferProjectionInverse;
    uniform mat4 gbufferModelView, gbufferModelViewInverse;

    uniform vec2 pixelSize;
    uniform vec2 viewDimensions;

    uniform float aspectRatio;
    uniform float centerDepthSmooth;
    uniform float near, far;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/unity/fragment/Camera.fsh"
    #include "/local/fragment/Toning.fsh"
    #include "/unity/shared/TextureBicubic.glsl"
    #include "/local/fragment/CameraDoF.fsh"

    // FUNC
    vec3 GetBloomTile(vec2 screenCoord, vec2 lod, vec2 offset) {
        return DecodeRGBE8(bicubic(colortex5, screenCoord * lod.y + offset));
    }

    vec3 CalculateBloomTiles(vec2 screenCoord) {
        const int tiles = 6;
        const float rTiles = 1.0 / tiles;

        const vec2 lods[tiles] = vec2[tiles](
            vec2(2.0, exp2(-2.0)),
            vec2(3.0, exp2(-3.0)),
            vec2(4.0, exp2(-4.0)),
            vec2(5.0, exp2(-5.0)),
            vec2(6.0, exp2(-6.0)),
            vec2(7.0, exp2(-7.0))
            //vec2(8.0, exp2(8.0))
        );

        const vec4 offsets[tiles] = vec4[tiles]( // offset.x, offset.y, pixelSizeMult.x, pixelSizeMult.y
            vec4(0.0),
            vec4(0.0, 0.25, 0.0, 2.0),
            vec4(0.125, 0.25, 2.0, 2.0),
            vec4(0.1875, 0.25, 4.0, 2.0),
            vec4(0.125, 0.3125, 2.0, 4.0),
            vec4(0.140625, 0.3125, 4.0, 4.0)
        );

        vec3 bloomTiles = vec3(0.0);

        for(int i = 0; i < tiles; ++i) {
            bloomTiles += GetBloomTile(screenCoord, lods[i], offsets[i].xy + pixelSize * offsets[i].zw);
        }

        return bloomTiles * rTiles;
    }

    // MAIN

    void main() {
        vec4 image = DecodeColour(texture2D(colortex3, screenCoord));
        float EV   = CalculateExposure(image.a);
        image.rgb  = CalculateCameraDoF(image.rgb, screenCoord);
    #ifdef BLOOM
        image.rgb += CalculateBloomTiles(screenCoord) * exp2(EV - BLOOM_EV_OFFSET - 4.0);
    #endif
        image.rgb *= EV;
        image.rgb  = Tonemap(image.rgb);
        //image.rgb  = Gamma(image.rgb);
        //image.rgb  = Lift(image.rgb);

    #ifdef LENS_PREVIEW
        if(all(lessThanEqual(screenCoord / vec2(1.0, aspectRatio), vec2(0.125)))) {
            image.rgb = texture2D(colortex0, (screenCoord * 2.0 + vec2(0.5, 0.95)) / vec2(1.0, aspectRatio)).rgb;
        }
    #endif

        fragColour = vec4(ToGamma(image.rgb), 1.0);
    }

#endif
