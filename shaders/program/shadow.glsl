/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    flat out mat3 tbn;

    out vec3 albedoTint;

    out vec3 worldPosition;

    out vec2 uvCoord;
    out vec2 entity;

    // IN
    attribute vec4 at_tangent;
    attribute vec3 mc_Entity;
    attribute vec2 mc_midTexCoord;

    // UNIFORM
    uniform mat4 shadowProjection;
    uniform mat4 shadowModelView, shadowModelViewInverse;

    uniform vec3 cameraPosition;

    uniform vec2 taaJitter;

    uniform float frameTimeCounter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/unity/shared/ShadowConversion.glsl"
    #include "/local/vertex/VertexDisplacement.vsh"

    // FUNC
    // MAIN

    void main() {
        uvCoord = gl_MultiTexCoord0.xy;

        entity = mc_Entity.xy;

        vec3 viewPosition  = transMAD(gl_ModelViewMatrix, gl_Vertex.xyz);
        worldPosition = transMAD(shadowModelViewInverse, viewPosition) + cameraPosition;
        worldPosition = CalculateVertexDisplacement(worldPosition, ivec2(mc_Entity.xz));

        viewPosition = transMAD(shadowModelView, worldPosition - cameraPosition);

        vec4 vertexPosition = viewPosition.xyzz * diagonal4(gl_ProjectionMatrix) + gl_ProjectionMatrix[3];
        vertexPosition.xyz *= vec3(vec2(CalculateShadowDistortionMult(vertexPosition.xy)), shadowDepthMult);

        gl_Position = vertexPosition;

        albedoTint = gl_Color.rgb;

        vec3 tangent = gl_NormalMatrix * (at_tangent.xyz / at_tangent.w);
        vec3 normal = gl_NormalMatrix * gl_Normal;
        tbn = mat3(tangent, cross(tangent, normal), normal);
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    // OUT
    layout(location = 0) out vec4 shadow0;
    layout(location = 1) out vec4 shadow1;

    // IN
    flat in mat3 tbn;

    in vec3 albedoTint;

    in vec3 worldPosition;

    in vec2 uvCoord;
    in vec2 entity;

    // UNIFORM
    uniform sampler2D tex;
    uniform sampler2D normals;

    uniform sampler2D shadowtex1;

    uniform sampler2D noisetex;

    uniform float frameTimeCounter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/fragment/WaterWaves.fsh"

    // FUNC
    float CalculateCaustic(vec3 originalPosition, vec3 normal) {
        const float depthMult = 4.0;
        const float rDepthMult = 1.0 / depthMult;

        float surfaceDepth = texture2D(shadowtex1, gl_FragCoord.xy * rShadowResolution).x;
        float dist = min(7.0, (surfaceDepth - gl_FragCoord.z) * shadowDepthBlocks);

        vec3 refractedPosition = refract(normalize(originalPosition), normal, 0.75 * rDepthMult) * dist + originalPosition;

        #define oldArea ( flength(dFdx(originalPosition)) * flength(dFdy(originalPosition)) )
        #define newArea ( flength(dFdx(refractedPosition)) * flength(dFdy(refractedPosition)) )

        return abs(oldArea / newArea);

        #undef oldArea
        #undef newArea
    }

    // MAIN

    void main() {
        if(!gl_FrontFacing) discard;

        bool isWater = entity.x == 10012.0;
        bool isCustomTranslucent = entity.x == 10015.0;

        vec4 albedo = texture2D(tex, uvCoord) * vec4(albedoTint, 1.0);
        if(albedo.a < 0.1 && !isWater && !isCustomTranslucent) discard;

        vec3 normal = texture2D(normals, uvCoord).xyz;
             normal = dot(normal, normal) <= 0.0 ? vec3(0.5, 0.5, 1.0) : normal;
        #ifdef TEXTURE_PACK_NO_NORMALS
             normal = vec3(0.5, 0.5, 1.0);
        #endif
             normal = normal * 2.0 - 1.0;
        if(isWater) {
             normal = CalculateWaterNormal(worldPosition);
        }
             normal = tbn * normal;

        if(isWater) {
            float caustics = CalculateCaustic(worldPosition, normal);
            albedo = vec4(vec3(ToGamma(caustics)), 1.0);
        }

        shadow0 = vec4(albedo.rgb * 0.25, albedo.a);
        shadow1 = vec4(normal * 0.5 + 0.5, float(isWater) * 0.5 + 0.5);
    }

#endif
