/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out vec3 albedoTint;

    out vec2 uvCoord;

    // IN
    // UNIFORM
    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"

    // FUNC
    // MAIN

    void main() {
        return;
        /*
        gl_Position = transMAD(gl_ModelViewMatrix, gl_Vertex.xyz).xyzz * diagonal4(gl_ProjectionMatrix) + gl_ProjectionMatrix[3];

        albedoTint = gl_Color.rgb;

        uvCoord = gl_MultiTexCoord0.xy;
        */
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    // OUT
    /* DRAWBUFFERS: */
    layout(location = 0) out vec4 gbuffer0;

    // IN
    flat in mat3 tbn;

    in vec3 albedoTint;

    in vec2 uvCoord;

    // UNIFORM
    uniform sampler2D tex;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"

    // FUNC
    // MAIN

    void main() {
        discard;
        /*
        vec4 albedo = vec4(albedoTint, 1.0);
        #if defined gbuffers_skytextured
        albedo.rgb *= texture2D(tex, uvCoord).rgb;
        #endif

        gbuffer0 = albedo;
        */
    }

#endif
