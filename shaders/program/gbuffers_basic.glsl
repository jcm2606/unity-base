/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out vec3 albedoTint;

    // IN
    // UNIFORM
    uniform mat4 gbufferModelView, gbufferModelViewInverse;

    uniform vec2 taaJitter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"

    // FUNC
    // MAIN

    void main() {
        vec4 vertexPosition = transMAD(gl_ModelViewMatrix, gl_Vertex.xyz).xyzz * diagonal4(gl_ProjectionMatrix) + gl_ProjectionMatrix[3];
    #if defined TAA
        vertexPosition.xy   = taaJitter * vertexPosition.w + vertexPosition.xy;
    #endif

        gl_Position = vertexPosition;

    #ifdef CUSTOM_BLOCK_HIGHLIGHT
        albedoTint = blockHighlightColour;
    #else
        albedoTint = gl_Color.rgb;
    #endif
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    // OUT
    /* DRAWBUFFERS:0 */
    layout(location = 0) out vec4 gbuffer0;

    // IN
    in vec3 albedoTint;

    // UNIFORM
    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"

    // FUNC
    // MAIN

    void main() {
        vec4 albedo = vec4(albedoTint, 1.0);

        gbuffer0 = albedo;
    }

#endif
