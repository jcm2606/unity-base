/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out vec2 screenCoord;

    // IN
    // UNIFORM
    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    // FUNC
    // MAIN

    void main() {
        gl_Position = ftransform();

        screenCoord = gl_Vertex.xy;
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    // OUT
    // IN
    in vec2 screenCoord;
    
    // UNIFORM
    uniform sampler2D colortex4;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE

    // FUNC
    // MAIN

    void main() {
        vec3 image = ToLinear(texture2D(colortex4, screenCoord).rgb);

        gl_FragColor = vec4(ToGamma(image), 1.0);
    }

#endif
