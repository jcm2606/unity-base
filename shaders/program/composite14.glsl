/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out vec2 screenCoord;

    // IN
    // UNIFORM
    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    // FUNC
    // MAIN

    void main() {
        gl_Position = ftransform();

        screenCoord = gl_Vertex.xy;
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    const bool colortex3MipmapEnabled = true;

    // OUT
    /* DRAWBUFFERS:350 */
    layout(location = 0) out vec4 feedback;
    layout(location = 1) out vec4 bloomTiles;
    layout(location = 2) out vec4 lensShape;

    // IN
    in vec2 screenCoord;
    
    // UNIFORM
    uniform sampler2D colortex3;
    uniform sampler2D colortex4;

    uniform sampler2D depthtex0;
    uniform sampler2D depthtex1;
    uniform sampler2D depthtex2;

    uniform mat4 gbufferProjection, gbufferProjectionInverse;
    uniform mat4 gbufferModelView, gbufferModelViewInverse;
    uniform mat4 gbufferPreviousProjection;
    uniform mat4 gbufferPreviousModelView;

    uniform vec3 cameraPosition;
    uniform vec3 previousCameraPosition;

    uniform vec2 viewDimensions;
    uniform vec2 pixelSize;
    uniform vec2 taaJitter;

    uniform float frameTime;
    uniform float aspectRatio;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/unity/fragment/TAA.fsh"
    #include "/local/fragment/CameraLens.fsh"

    // FUNC
    vec3 CalculateBloomTile(vec2 screenCoord, const vec2 lod, const vec2 offset) {
        vec2 coord = (screenCoord - offset) * lod.y;
        vec2 scale = pixelSize * lod.y;

        if(any(greaterThanEqual(abs(coord - 0.5), scale + 0.5))) return vec3(0.0);

        const int samples = 4;
        const float rSamples = 1.0 / float(samples + 1.0);

        vec3 bloom = vec3(0.0);
        float totalWeight = 0.0;

        for(int i = -samples; i <= samples; ++i) {
            for(int j = -samples; j <= samples; ++j) {
                float sampleLength = 1.0 - flength(vec2(i, j)) * rSamples;
                float sampleWeight = saturate(pow(sampleLength, 4.0));

                bloom += DecodeColour(texture2DLod(colortex3, coord + vec2(i + 0.3, j) * scale, lod.x).rgb) * sampleWeight;
                totalWeight += sampleWeight;
            }
        }

        return bloom / totalWeight;
    }

    vec3 CalculateBloomTiles(vec2 screenCoord) {
        const int tiles = 6;

        const vec2 lods[tiles] = vec2[tiles](
            vec2(2.0, exp2(2.0)),
            vec2(3.0, exp2(3.0)),
            vec2(4.0, exp2(4.0)),
            vec2(5.0, exp2(5.0)),
            vec2(6.0, exp2(6.0)),
            vec2(7.0, exp2(7.0))
            //vec2(8.0, exp2(8.0))
        );

        const vec4 offsets[tiles] = vec4[tiles]( // offset.x, offset.y, pixelSizeMult.x, pixelSizeMult.y
            vec4(0.0),
            vec4(0.0, 0.25, 0.0, 2.0),
            vec4(0.125, 0.25, 2.0, 2.0),
            vec4(0.1875, 0.25, 4.0, 2.0),
            vec4(0.125, 0.3125, 2.0, 4.0),
            vec4(0.140625, 0.3125, 4.0, 4.0)
        );

        vec3 bloomTiles = vec3(0.0);

        for(int i = 0; i < tiles; ++i) {
            bloomTiles += CalculateBloomTile(screenCoord, lods[i], offsets[i].xy + pixelSize * offsets[i].zw);
        }

        return bloomTiles;
    }

    float CalculateAverageLuma() {
        #define currLuma luma(max0(DecodeColour(textureLod(colortex3, vec2(0.5), 10).rgb)))
        #define prevLuma DecodeColour(texture2D(colortex3, vec2(0.5)).a)
        
        return mix(prevLuma, currLuma, clamp(frameTime, 0.0, 0.99));

        #undef currLuma
        #undef prevLuma
    }

    // MAIN

    void main() {
        feedback = EncodeColour(vec4(CalculateTAA(screenCoord), CalculateAverageLuma()));
    #ifdef BLOOM
        bloomTiles = EncodeRGBE8(CalculateBloomTiles(screenCoord));
    #endif
        lensShape = vec4(CalculateCameraLensShape(screenCoord), 1.0);
    }

#endif
