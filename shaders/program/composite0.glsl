/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    out mat3x4 skySH;

    out vec3 lightColour;
    out vec3 skyColour;

    out vec2 screenCoord;

    // IN
    // UNIFORM
    uniform sampler2D noisetex;

    uniform mat4 gbufferModelViewInverse;

    uniform vec3 sunDirection;

    uniform float wetness;
    uniform float eyeAltitude;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/unity/shared/PhysicalSky.glsl"
    #include "/unity/vertex/PhysicalSkySH.vsh"

    // FUNC
    // MAIN

    void main() {
        gl_Position = ftransform();

        screenCoord = gl_Vertex.xy;

        lightColour = (GetSunColorZom(sunDirection) + GetMoonColorZom(-sunDirection));

        vec3 viewAbsorb = vec3(1.0);
        skyColour = sky_atmosphere(vec3(0.0), vec3(0.0, 1.0, 0.0), vec3(0.0, 1.0, 0.0), sunDirection, -sunDirection, skySunColor, skyMoonColor, 10, viewAbsorb);

        CalculateSkySH(viewAbsorb, skyColour);
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    const bool generateShadowMipmap = true;

    // OUT
    /* DRAWBUFFERS:4 */
    layout(location = 0) out vec4 fragColour;

    // IN
    in mat3x4 skySH;

    in vec3 lightColour;

    in vec2 screenCoord;
    
    // UNIFORM
    uniform sampler2D colortex0;
    uniform sampler2D colortex1;
    uniform sampler2D colortex2;
    uniform sampler2D colortex4;
    uniform sampler2D colortex5;
    uniform sampler2D colortex6;

    uniform sampler2D depthtex0;
    uniform sampler2D depthtex1;

    uniform sampler2D shadowtex0;
    uniform sampler2D shadowtex1;
    uniform sampler2D shadowcolor0;
    uniform sampler2D shadowcolor1;

    uniform sampler2D noisetex;

    uniform mat4 gbufferProjection, gbufferProjectionInverse;
    uniform mat4 gbufferModelView, gbufferModelViewInverse;
    uniform mat4 shadowProjection;
    uniform mat4 shadowModelView;

    uniform vec3 sunDirection, sunDirectionView;
    uniform vec3 shadowLightDirection, shadowLightDirectionView;

    uniform vec2 taaJitter;

    uniform float near, far;
    uniform float frameTimeCounter;
    uniform float sunAngle;
    uniform float wetness;
    uniform float eyeAltitude;

    uniform int frameCounter;
    uniform int isEyeInWater;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/InternalSettings.glsl"
    
    #include "/local/shared/Material.glsl"
    #include "/unity/shared/Position.glsl"
    #include "/unity/shared/PhysicalSky.glsl"
    #include "/local/fragment/DiffuseLighting.fsh"
    #include "/local/fragment/SpecularLighting.fsh"

    // FUNC
    vec3 CalculateRefractedScreenPosition(Position positionBack, Position positionFront, vec3 normal, float eta, float strength) {
        vec3 viewVector = normalize(-positionFront.viewPosition);
        vec3 flatNormal = normalize(cross(dFdx(positionFront.viewPosition), dFdy(positionFront.viewPosition)));
        vec3 rayDirection = refract(viewVector, mat3(gbufferModelView) * normal - flatNormal, eta);

        vec3 refractedPosition   = rayDirection * abs(distance(positionBack.viewPosition, positionFront.viewPosition) * strength) / positionFront.viewPosition.z + positionFront.viewPosition;
             refractedPosition   = ViewToScreenPosition(refractedPosition);
             refractedPosition.z = texture2D(depthtex1, refractedPosition.xy).x;

        return refractedPosition;
    }

    vec3 CalculateAnalyticalWaterFog(Position positionBack, Position positionFront, vec3 diffuse) {
        const float density = 2.0;
        const vec3 scatterCoefficient = vec3(0.005) * rLOG2;
        const vec3 absorbCoefficient = vec3(0.25422, 0.03751, 0.01150) * rLOG2;
        const vec3 transmittanceCoefficient = scatterCoefficient + absorbCoefficient;

        float dist = (isEyeInWater == 0) ? fdistance(positionFront.scenePosition, positionBack.scenePosition) : fdistance(vec3(0.0), positionFront.scenePosition);
        float opticalDepth = density * dist;

        return diffuse * exp2(-transmittanceCoefficient * opticalDepth);
    }

    // MAIN

    void main() {
        vec2 coord = screenCoord;
        
        Position positionBack  = GetPosition(coord, depthtex1);
        Position positionFront = GetPosition(coord, depthtex0);

        Material material = GetMaterial(coord);

        vec3 image = DecodeRGBE8(texture2D(colortex4, coord));

        bool isTransparent = positionBack.depth > positionFront.depth;

        vec3 refractedPosition = CalculateRefractedScreenPosition(positionBack, positionFront, material.normal, 0.75, 1.0);

        if(refractedPosition.z > texture2D(depthtex0, refractedPosition.xy).x) {
            coord = refractedPosition.xy;
            positionBack = GetPosition(coord, depthtex1);
            image = DecodeRGBE8(texture2D(colortex4, coord));
            isTransparent = positionBack.depth > positionFront.depth;
        }

        vec2 dither = vec2(Bayer16(gl_FragCoord.xy), 256.0);
    #ifdef TAA
        dither.x = fract(dither.x + LinearBayer16(frameCounter));
    #endif

        if(!isLand(positionBack.depth)) {
            vec3 sceneDirection = normalize(positionBack.scenePosition);

            vec3 viewAbsorb = vec3(0.0);
            image = sky_atmosphere(vec3(CalculateStars(sceneDirection)), sceneDirection, vec3(0.0, 1.0, 0.0), sunDirection, -sunDirection, skySunColor, skyMoonColor, 15, viewAbsorb);

            float VoS = dot(sceneDirection, sunDirection);
            image += CalculateSunSpot(VoS) * skySunColor * viewAbsorb;
            image += CalculateMoonSpot(-VoS) * skyMoonColor * viewAbsorb;
        }

        if(material.masks.water || isEyeInWater == 1) image = CalculateAnalyticalWaterFog(positionBack, positionFront, image);

        vec3 shadows = texture2D(colortex5, screenCoord).rgb;

        if(isTransparent && !material.masks.water) {
            vec4 transparentGeometry = material.albedo;
            transparentGeometry.rgb  = CalculateDiffuseLighting(material, positionFront.scenePosition, dither, shadows);

            image = mix(image * material.albedo.rgb, transparentGeometry.rgb, transparentGeometry.a);
        }

        if(isLand(positionFront.depth) && isEyeInWater == 0) {
            image = CalculateSpecularLighting(positionFront, material, image, shadows, dither);
        }

        fragColour = EncodeRGBE8(image);
    }

#endif
