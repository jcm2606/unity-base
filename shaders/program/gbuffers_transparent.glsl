/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#include "/unity/Syntax.glsl"

#if   defined vsh

    // VERTEX

    // CONST
    // OUT
    flat out mat3 tbn;

    out vec3 albedoTint;
    out vec3 tangentDirection;
    out vec3 tangentDirectionView;
    out vec3 worldPosition;

    out vec2 uvCoord;
    out vec2 lmCoord;

    out float defaultAO;

    flat out ivec2 blockID;
    flat out int maskID;

    // IN
    attribute vec4 at_tangent;
    attribute vec3 mc_Entity;
    attribute vec2 mc_midTexCoord;

    // UNIFORM
    uniform mat4 gbufferModelView, gbufferModelViewInverse;

    uniform vec3 cameraPosition;

    uniform vec2 taaJitter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"

    // FUNC
    // MAIN

    void main() {
        vec3 viewPosition  = transMAD(gl_ModelViewMatrix, gl_Vertex.xyz);
        worldPosition = transMAD(gbufferModelViewInverse, viewPosition) + cameraPosition;

        viewPosition = transMAD(gbufferModelView, worldPosition - cameraPosition);

        vec4 vertexPosition = viewPosition.xyzz * diagonal4(gl_ProjectionMatrix) + gl_ProjectionMatrix[3];
    #if defined TAA
        vertexPosition.xy   = taaJitter * vertexPosition.w + vertexPosition.xy;
    #endif

        gl_Position = vertexPosition;

        albedoTint = gl_Color.rgb;
    #if !defined gbuffers_hand_water
        defaultAO  = gl_Color.a;
    #else
        defaultAO = 0.5;
    #endif

        uvCoord = gl_MultiTexCoord0.xy;
        lmCoord = gl_MultiTexCoord1.xy * 0.00390625;

        blockID = ivec2(mc_Entity.xy);
        maskID = CalculateMaskID(mc_Entity.xz);

        vec3 normal  = gl_Normal;
        vec3 tangent = at_tangent.xyz / at_tangent.w;

        tbn = mat3(tangent, cross(tangent, normal), normal);

        tangentDirection = -normalize((viewPosition * gl_NormalMatrix) * tbn);
        tangentDirectionView = (mat3(gbufferModelViewInverse) * viewPosition) * tbn;
    }

#elif defined fsh

    // FRAGMENT

    // CONST
    // OUT
    /* DRAWBUFFERS:120 */
    layout(location = 2) out vec4 gbuffer0;
    layout(location = 0) out vec4 gbuffer1;
    layout(location = 1) out vec4 gbuffer2;

    // IN
    flat in mat3 tbn;

    in vec3 albedoTint;
    in vec3 tangentDirection;
    in vec3 tangentDirectionView;
    in vec3 worldPosition;

    in vec2 uvCoord;
    in vec2 lmCoord;

    in float defaultAO;

    flat in ivec2 blockID;
    flat in int maskID;

    // UNIFORM
    uniform sampler2D tex;
    uniform sampler2D normals;
    uniform sampler2D specular;

    uniform sampler2D noisetex;

    uniform float frameTimeCounter;

    // GLOBAL
    // STRUCT
    // UTILITY
    #include "/unity/Utility.glsl"

    // INCLUDE
    #include "/local/shared/MaskID.glsl"

    #include "/local/fragment/TexturePack.fsh"
    #include "/local/fragment/WaterWaves.fsh"

    // FUNC
    vec3 ClampNormal(vec3 n, vec3 v){
        float NoV = saturate(dot(n, -v));
        return normalize(NoV * v + n);
    }

    // MAIN

    void main() {
        //vec2 texCoord = uvCoord;

        vec4 albedo = texture2D(tex, uvCoord) * vec4(albedoTint, 1.0);
        float alphaMask = ceil(albedo.a);

        if(maskID == MASKID_WATER) albedo = vec4(0.0);

        vec3 normal = texture2D(normals, uvCoord).xyz;
        #ifdef TEXTURE_PACK_NO_NORMALS
             normal = vec3(0.5, 0.5, 1.0);
        #endif
             normal = normalize(normal * 2.0 - 1.0);
        if(maskID == MASKID_WATER) {
             normal = CalculateWaterNormal(CalculateWaterParallax(worldPosition, tangentDirectionView));
        }
             normal = tbn * ClampNormal(normal, -tangentDirectionView);

        vec4 specularData = texture2D(specular, uvCoord);

        float roughness, reflectance, emission;
        ReadSpecularMap(specularData, blockID, roughness, reflectance, emission);

        gbuffer0 = albedo;
        gbuffer1 = vec4(EncodeNormal(normal), Encode3x8(vec3(roughness, reflectance, emission)), 0.0, alphaMask);
        gbuffer2 = vec4(Encode2x8(lmCoord), 0.0, Encode2x8(vec2(defaultAO, 1.0)), Encode2x8(vec2(float(maskID) * maskMult, alphaMask)));
    }

#endif
