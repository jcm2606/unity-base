/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_CAMERA
    #define LOCAL_INCL_SETTINGS_CAMERA

    /*** Camera Settings ***/

    #define CAMERA_MODE_AUTO 0
    #define CAMERA_MODE_MANUAL 1
    #define CAMERA_MODE CAMERA_MODE_AUTO // Which mode should the camera operate in?. Automatic adjusts everything based off on the average brightness of the image, automatically adjusting the exposure as needed. Manual allows you to control the exact settings the camera uses. [CAMERA_MODE_AUTO CAMERA_MODE_MANUAL]

    #define CAMERA_FOCAL_LENGTH 40 // [10 20 30 40 50 60 70 80 90 100 120 140 160 180 200 220 240 260 280 300 320 340 360 380 400 420 440 460 480 500]
    #define CAMERA_APERTURE 2.8 // [1.4 2 2.8 4 5.6 8 11 16 22]
    #define CAMERA_SHUTTER_SPEED 1600 // [1 2 4 8 10 20 25 50 75 100 125 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000]
    #define CAMERA_ISO 50 // [50 75 100 200 400 800 1600 3200 6400]
    #define CAMERA_EV_OFFSET 0.0 // [-4.0 -3.9 -3.8 -3.7 -3.6 -3.5 -3.4 -3.3 -3.2 -3.1 -3.0 -2.9 -2.8 -2.7 -2.6 -2.5 -2.4 -2.3 -2.2 -2.1 -2.0 -1.9 -1.8 -1.7 -1.6 -1.5 -1.4 -1.3 -1.2 -1.1 -1.0 -0.9 -0.8 -0.7 -0.6 -0.5 -0.4 -0.3 -0.2 -0.1 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0]

    #define CAMERA_FOCUS_MODE_AUTO 0
    #define CAMERA_FOCUS_MODE_MANUAL 1
    #define CAMERA_FOCUS_MODE CAMERA_FOCUS_MODE_AUTO // Which mode should the camera use for focusing?. Automatic focuses on whatever you're looking at, with the option to offset it by a number of blocks. Manual allows you to set the exact distance which the camera focuses at. Use auto when you need fine control, see manual focus distance description. [CAMERA_FOCUS_MODE_AUTO CAMERA_FOCUS_MODE_MANUAL]

    #define CAMERA_MANUAL_FOCUS_DISTANCE 72 // How far away should the camera focus at when in manual focus mode?. The slider only offers increments of 8 blocks, so for fine control over the focus distance for close up shots, switch the camera to auto focus mode and use the auto focus offset. [8 16 24 32 40 48 56 64 72 80 88 96 104 112 120 128 136 144 152 160 168 176 184 192 200 208 216 224 232 240 248 256]

    #define CAMERA_AUTO_FOCUS_OFFSET 0 // How much should the focus distance be offset when the camera is in auto focus mode?. Positive values push focused point further away, negative values bring the focused point further in. [-16 -15 -14 -13 -12 -11 -10 -9 -8 -7 -6 -5 -4.5 -4 -3.5 -3 -2.5 -2 -1.5 -1 -0.5 0 0.5 1 1.5 2 2.5 3 3.5 4 4.5 5 6 7 8 9 10 11 12 13 14 15 16]

    /*** Depth of Field Settings ***/

    //#define DOF // Depth of Field simulates the natural blurring that occurs when the camera focuses on an object.
    #define DOF_SAMPLES 64 // How many samples should Depth of Field use?. More samples results in a crisper, more accurate blur, at the cost of performance. [32 64 128 192 256 320 384 512 768 1024]
    #define DOF_DISTORTION_ANAMORPHIC 1.0
    #define DOF_DISTORTION_BARREL 0.6

    const float centerDepthHalflife = 0.9; // How long should automatic camera focus take to adjust?. [0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.2 2.4 2.6 2.8 3.0]

    /*** Lens Settings ***/

    #define LENS_BLADES 7 // How many blades should the camera's lens use?. [1 3 4 5 6 7 8 9 10 11]
    #define LENS_ROTATION 11.25 // How should the blades be rotated?. [0.00 11.25 22.50 33.75 45.00 56.25 67.50 78.75 90.00 101.25 112.50 123.75 135.00 146.25 157.50 168.75 180.00 191.25 202.50 213.75 225.00 236.25 247.50 258.75 270.00 281.25 292.50 303.75 315.00 326.25 337.50 348.75]
    #define LENS_ROUNDING 0.50 // How rounded should the blades be?. [0.00 0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.00]
    #define LENS_SOFTNESS 0.60 // How soft should the resulting bokeh be?. [0.00 0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.00]
    #define LENS_SHIFT 0.05 // [0.00 0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.00]
    #define LENS_BIAS 0.85 // [0.00 0.05 0.10 0.15 0.20 0.25 0.30 0.35 0.40 0.45 0.50 0.55 0.60 0.65 0.70 0.75 0.80 0.85 0.90 0.95 1.00]

    //#define LENS_PREVIEW // Adds an overlay in the bottom left corner, showing a preview of the lens.

    #define LENS_BUFFER_SCALE 0.25
    #define LENS_BUFFER_OFFSET vec2(0.5, 0.5)

    /*** Bloom Settings ***/

    #define BLOOM // Bloom simulates the glare and feathering that occurs when a dark area is near a disproportionatly bright area.
    #define BLOOM_EV_OFFSET 0.0 // [-4.0 -3.9 -3.8 -3.7 -3.6 -3.5 -3.4 -3.3 -3.2 -3.1 -3.0 -2.9 -2.8 -2.7 -2.6 -2.5 -2.4 -2.3 -2.2 -2.1 -2.0 -1.9 -1.8 -1.7 -1.6 -1.5 -1.4 -1.3 -1.2 -1.1 -1.0 -0.9 -0.8 -0.7 -0.6 -0.5 -0.4 -0.3 -0.2 -0.1 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0 2.1 2.2 2.3 2.4 2.5 2.6 2.7 2.8 2.9 3.0 3.1 3.2 3.3 3.4 3.5 3.6 3.7 3.8 3.9 4.0]

#endif
