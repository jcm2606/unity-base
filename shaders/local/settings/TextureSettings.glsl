/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_TEXTURE
    #define LOCAL_INCL_SETTINGS_TEXTURE

    /*** Texture Material Settings ***/

    #define GREYSCALE 0
    #define PBR_OLD 1
    #define PBR_LAB 2
    #define PBR_CONTINUUM 3
    #define TEXTURE_PACK_FORMAT PBR_OLD // Which texture pack format should the shader use?. 'Greyscale' is the old smoothness-only format. 'Old PBR' is the old PBR smoothness/metalness/emission format. 'shaderLABs PBR' is the new PBR format being developed as the new standard. 'Continuum PBR' is the new PBR format used by the Pulchra & Stratum resource packs. [GREYSCALE PBR_OLD PBR_LAB PBR_CONTINUUM]
    
    //#define TEXTURE_PACK_SQR_ROUGHNESS

    //#define TEXTURE_PACK_NO_NORMALS // Should the shader ignore the texture pack's normal map?. The official option provided by Optifine in the shader selection menu does the same thing, but it has to reload both the shader pack and the texture pack. This option only needs to reload the shader pack, so toggling this option is much, much quicker.

    //#define TEXTURE_PACK_NO_ALBEDO // Should the shader ignore the texture pack's albedo map?. This is basically the typical 'White World' option, mainly used to debug lighting and AO.

    //#define CUSTOM_BLOCK_HIGHLIGHT
    #define CUSTOM_BLOCK_HIGHLIGHT_R 255 // How strong should the block light source's reds be?. Only works when in 'RGB' colouring mode. [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255]
    #define CUSTOM_BLOCK_HIGHLIGHT_G 255 // How strong should the block light source's greens be?. Only works when in 'RGB' colouring mode. [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255]
    #define CUSTOM_BLOCK_HIGHLIGHT_B 255 // How strong should the block light source's blues be?. Only works when in 'RGB' colouring mode. [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61 62 63 64 65 66 67 68 69 70 71 72 73 74 75 76 77 78 79 80 81 82 83 84 85 86 87 88 89 90 91 92 93 94 95 96 97 98 99 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255]

    const vec3 blockHighlightColour = vec3(CUSTOM_BLOCK_HIGHLIGHT_R, CUSTOM_BLOCK_HIGHLIGHT_G, CUSTOM_BLOCK_HIGHLIGHT_B) / 255.0;

#endif
