/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_SKY
    #define LOCAL_INCL_SETTINGS_SKY

    /*** Atmosphere Settings ***/

    const float skyDomeQuality  = 0.5;
    const float rSkyDomeQuality = 1.0 / skyDomeQuality;

    #define SKY_SUN_LUMINANCE  1380000.0 // Value in LUX. Default is a known good value.
    #define SKY_MOON_LUMINANCE 2.0 // Value in LUX. Default is a known good value.

    #define SKY_SUN_TEMPERATURE 5778 // [4000 4250 4500 4750 5000 5250 5500 5750 5778 6000 6250 6500]

    #define skySunColor Blackbody(SKY_SUN_TEMPERATURE) * SKY_SUN_LUMINANCE
    #define skyMoonColor SaturationMod(vec3(0.0, 0.0, 1.0), 0.1) * SKY_MOON_LUMINANCE

    const float sunAngularSize = 0.535;
    const float sunAngularDiameter = radians(sunAngularSize);
    const float sunAngularRadius   = 0.5 * sunAngularDiameter;

    const float moonAngularSize = 0.528;
    const float moonAngularDiameter = radians(moonAngularSize);
    const float moonAngularRadius   = 0.5 * moonAngularDiameter;

    const float atmospherePlanetRadius = 6731e3; // Should probably move this to somewhere else.

    #define SKY_ATMOSPHERE_HEIGHT 110e3

    const float atmosphereMieG = 0.77;

    #define SKY_AIR_NUMBER_DENSITY       2.6867774e19 // Couldn't find it for air, so just using value for an ideal gas. Not sure how different it is for actual air.
    #define SKY_OZONE_PEAK_CONCENTRATION 8e-6
    #define SKY_OZONE_CROSS_SECTION      vec3(4.51103766177301E-21, 3.2854797958699E-21, 1.96774621921165E-22)

    const vec3 atmosphereRayleighCoefficient = vec3(4.593e-6, 1.097e-5, 2.716e-5);
    #define SKY_ATMOSPHERE_OZONE_COEFFICIENT   SKY_OZONE_CROSS_SECTION * (SKY_AIR_NUMBER_DENSITY * SKY_OZONE_PEAK_CONCENTRATION) // Might need to have this be a constant again.
    const vec3 atmosphereMieCoefficient      = vec3(2.0e-6); // Should be >= 2e-6, depends heavily on conditions. Current value is just one that looks good.

    // The rest of these constants are set based on the above constants
    const vec2  atmosphereInverseScaleHeights     = 1.0 / vec2(8.0e3, 1.2e3); //atmosphereScaleHeights
    const vec2  atmosphereScaledPlanetRadius      = atmospherePlanetRadius * atmosphereInverseScaleHeights;
    const float atmosphereRadius                  = atmospherePlanetRadius + SKY_ATMOSPHERE_HEIGHT;
    const float atmosphereRadiusSquared           = atmosphereRadius * atmosphereRadius;

    const mat2x3 atmosphereScatteringCoefficients  = mat2x3(atmosphereRayleighCoefficient, atmosphereMieCoefficient);
    const mat3   atmosphereAttenuationCoefficients = mat3(atmosphereRayleighCoefficient, atmosphereMieCoefficient * 1.11, SKY_ATMOSPHERE_OZONE_COEFFICIENT); // commonly called the extinction coefficient

#endif
