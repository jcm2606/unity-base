/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS_TONING
    #define LOCAL_INCL_SETTINGS_TONING

    /*** Toning Settings ***/

    #define TONEMAP_TOE_STRENGTH    0.3 // -1 -> 1 [-1.0 -0.9 -0.8 -0.7 -0.6 -0.5 -0.4 -0.3 -0.2 -0.1 0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0]
    #define TONEMAP_TOE_LENGTH      0.2 // 0 -> 1 [0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0]
    #define TONEMAP_LINEAR_SLOPE    1   // Should usually be left at 1
    #define TONEMAP_LINEAR_LENGTH   0.5 // 0 -> 1 [0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0]
    #define TONEMAP_SHOULDER_CURVE  0.5 // 0 -> 1 [0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0]
    #define TONEMAP_SHOULDER_LENGTH 1   // Not currently in an actually useful state

    #define LIFT_R 0 // -50 -> 50
    #define LIFT_G 0 // -50 -> 50
    #define LIFT_B 0 // -50 -> 50

    #define GAMMA_CHROMINANCE 1.0 // 0.5 -> 2 // [0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]
    #define GAMMA_LUMINANCE   1.0 // 0.5 -> 2 // [0.5 0.6 0.7 0.8 0.9 1.0 1.1 1.2 1.3 1.4 1.5 1.6 1.7 1.8 1.9 2.0]

#endif
