/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_INTERNAL_SETTINGS
    #define LOCAL_INCL_INTERNAL_SETTINGS

/*
    const int colortex0Format = RGBA8;
    const int colortex1Format = RG32F;
    const int colortex2Format = RGBA16;
    const int colortex3Format = RGBA16F;
    const int colortex4Format = RGBA16; // High-Precision Image.
    const int colortex5Format = RGBA8; // Low-Precision Image.
    const int colortex6Format = RGBA16; // Skydome.

    const bool colortex3Clear = false;
*/

    const float sunPathRotation = -45.00; // At what angle should the path of the sun and moon be tilted at?. [-90.00 -78.75 -67.50 -56.25 -45.00 -33.75 -22.50 -11.25 0.00 11.25 22.50 33.75 45.00 56.25 67.50 78.75 90.00]

    const int noiseTextureResolution = 64;

    // Watermark used for crediting in options menu.
    #define WATERMARK AUTHOR
    #ifdef WATERMARK
    #endif

#endif
