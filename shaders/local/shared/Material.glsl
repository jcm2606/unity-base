/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SHARED_MATERIAL
    #define LOCAL_INCL_SHARED_MATERIAL

    #include "/local/shared/MaskID.glsl"

    // Masks.
    struct Mask {
        bool entity;
        bool stainedGlass;
        bool ice;
        bool water;
        bool emissive;
        bool fire;
        bool foliageHanging;
        bool foliageLeaves;
        bool foliageGround;
        bool foliage;
    };

    Mask GetMask(int maskID) {
        Mask mask;
        mask.foliageGround = maskID == MASKID_FOLIAGE_GROUND;
        mask.foliageLeaves = maskID == MASKID_FOLIAGE_LEAVES;
        mask.foliageHanging = maskID == MASKID_FOLIAGE_HANGING;
        mask.foliage = mask.foliageGround || mask.foliageLeaves || mask.foliageHanging;
        mask.fire = maskID == MASKID_FIRE;
        mask.emissive = maskID == MASKID_EMISSIVE;
        mask.water = maskID == MASKID_WATER;
        mask.ice = maskID == MASKID_ICE;
        mask.stainedGlass = maskID == MASKID_STAINED_GLASS;
        mask.entity = maskID == MASKID_ENTITY;

        return mask;
    }

    // Material.
    struct Material {
        vec4 albedo;
        vec3 normal;
        float blockLight;
        float blockLightShading;
        float skyLight;
        float skyLightShading;
        float defaultAO;
        float parallaxShadow;
        float roughness;
        float reflectance;
        float emission;
        int maskID;
        Mask masks;
    };

    vec4 GetAlbedo(vec2 screenCoord) {
        vec4 albedo = texture2D(colortex0, screenCoord);
        return vec4(ToLinear(albedo.rgb), albedo.a);
    }

    vec3 GetNormal(vec2 screenCoord) {
        return DecodeNormal(texture2D(colortex1, screenCoord).x);
    }

    Material GetMaterial(vec2 screenCoord) {
        mat3x4 gbufferData = mat3x4(
            texture2D(colortex0, screenCoord),
            texture2D(colortex1, screenCoord),
            texture2D(colortex2, screenCoord)
        );

        Material materialObject;

        materialObject.albedo = vec4(ToLinear(gbufferData[0].rgb), gbufferData[0].a);

        materialObject.normal = DecodeNormal(gbufferData[1].x);
        
        vec2 lightmap = Decode2x8(gbufferData[2].x);
        materialObject.blockLight = lightmap.x;
        materialObject.skyLight = lightmap.y;

        // TODO: Lightmap Shading.

        vec2 surfaceLighting = Decode2x8(gbufferData[2].z);
        materialObject.defaultAO = surfaceLighting.x;
        materialObject.parallaxShadow = surfaceLighting.y;
        
        vec3 specularData = Decode3x8(gbufferData[1].y);
        materialObject.roughness = specularData.x;
        materialObject.reflectance = specularData.y;
        materialObject.emission = specularData.z;

        materialObject.maskID = int(Decode2x8(gbufferData[2].w).x * maskTotal);
        materialObject.masks = GetMask(materialObject.maskID);

        return materialObject;
    }

#endif
