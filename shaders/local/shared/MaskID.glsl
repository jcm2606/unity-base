/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SHARED_MASK_ID
    #define LOCAL_INCL_SHARED_MASK_ID

    const int   maskTotal = 255;
    const float maskMult  = 1.0 / maskTotal;

    #define MASKID_ENTITY 64

    #define MASKID_STAINED_GLASS 34
    #define MASKID_ICE 33
    #define MASKID_WATER 32

    #define MASKID_EMISSIVE 20
    #define MASKID_FIRE 19
    #define MASKID_FOLIAGE_HANGING 18
    #define MASKID_FOLIAGE_LEAVES 17
    #define MASKID_FOLIAGE_GROUND 16

    #define MASKID_DEFAULT 0

    #if defined gbuffers || defined shadow
        int CalculateMaskID(vec2 entity) {
            int maskID = MASKID_DEFAULT;

            #if   defined gbuffers_terrain || defined gbuffers_water || defined gbuffers_hand || defined gbuffers_hand_water
                switch(int(entity.x)) {
                    case 10000: // Single Foliage.
                    case 10001: // Double Foliage Bottom.
                    case 10002: // Double Foliage Top.
                    case 10003: // Solid Foliage (No Waving).
                    case 10004: // Sapling.
                    case 10005: // Crop.
                                maskID = MASKID_FOLIAGE_GROUND; break;
                    case 10006: // Leaves.
                                maskID = MASKID_FOLIAGE_LEAVES; break;
                    case 10007: // Vines.
                                maskID = MASKID_FOLIAGE_HANGING; break;
                    case 10011: // Emissive Block.
                                maskID = MASKID_EMISSIVE; break;
                    case 10012: // Water.
                                maskID = MASKID_WATER; break;
                    default: maskID = MASKID_DEFAULT;
                }
            #elif defined gbuffers_entities
                maskID = MASKID_ENTITY;
            #endif

            return maskID;
        }
    #endif

#endif
