/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_CAMERA_LENS
    #define LOCAL_INCL_FRAG_CAMERA_LENS

    vec3 CalculateCameraLensShape(vec2 screenCoord) {
    #if !defined DOF && !defined LENS_PREVIEW
        return vec3(0.0);
    #endif
    
        const float lod = 2.0;
        const float lod2 = exp2(lod);

        const float a = TAU / LENS_BLADES;
        const mat2 rot = mat2(cos(a), -sin(a), sin(a), cos(a));

        const vec3 size = 0.4 * vec3(1.0 - vec2(LENS_SHIFT, LENS_SHIFT * 0.5), 1.0);
        const vec3 size0 = size * (1.0 - LENS_SOFTNESS);
        const vec3 size1 = size * (1.0 - LENS_SOFTNESS) * 0.8;

        vec2 coord = screenCoord * lod2 + (-LENS_BUFFER_OFFSET * lod2);

        float r = 0.0;

        const float rotation = radians(float(LENS_ROTATION));
        const vec2 caddv = vec2(sin(rotation), -cos(rotation));
        vec2 addv = caddv;

        vec2 centerOffset = coord - 0.5;

        for(int i = 0; i < LENS_BLADES; ++i) {
            addv = rot * addv;
            r = max(r, dot(addv, centerOffset));
        }

        r = mix(r, flength(centerOffset) * 0.8, LENS_ROUNDING);

        vec3 lensShape = saturate(1.0 - smoothstep(size0, size, vec3(r)));
             lensShape = lensShape * (1.0 - saturate(smoothstep(size, size1, vec3(r)) * LENS_BIAS));

        return lensShape;
    }

#endif
