/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_CAMERA_DOF
    #define LOCAL_INCL_FRAG_CAMERA_DOF

    #include "/unity/shared/SpaceConversion.glsl"

    float CalculateFocus(float depth) {
        const float focalLength = CAMERA_FOCAL_LENGTH / 1000.0;
        const float aperture = (CAMERA_FOCAL_LENGTH / CAMERA_APERTURE) / 1000.0;

        #if CAMERA_FOCUS_MODE == CAMERA_FOCUS_MODE_AUTO
            float focus = min0(ScreenToViewDepth(centerDepthSmooth) - CAMERA_AUTO_FOCUS_OFFSET);
        #elif CAMERA_FOCUS_MODE == CAMERA_FOCUS_MODE_MANUAL
            const float focus = float(-CAMERA_MANUAL_FOCUS_DISTANCE);
        #endif

        return aperture * (focalLength * (focus - depth)) / (focus * (depth - focalLength));
    }

    vec2 CalculateDistOffset(vec2 prep, float angle, vec2 offset) {
        float PoO = dot(prep, offset);
        //#define PoO dot(prep, offset) // Define might be a teeny tiny bit faster, but I'm not sure.
        return offset * angle + (prep * (PoO * -angle + PoO));
        //#undef PoO
    }

    vec3 CalculateCameraDoF(vec3 image, vec2 screenCoord) {
    #ifndef DOF
        return image;
    #endif

        if(texture2D(depthtex2, screenCoord).x > texture2D(depthtex1, screenCoord).x) return image;

        const int samples = DOF_SAMPLES;

        vec3 dof = vec3(0.0);
        vec3 weight = vec3(0.0);

        float r = 1.0;
        const mat2 rot = mat2(cos(GOLDEN), -sin(GOLDEN), sin(GOLDEN), cos(GOLDEN));

        const float aperture = (35.0 / CAMERA_APERTURE);

        float depth = ScreenToViewDepth(texture2D(depthtex0, screenCoord).x);
        float pcoc = CalculateFocus(depth);

        vec2 pcocAngle   = vec2(0.0, pcoc);
        vec2 sampleAngle = vec2(0.0, 1.0);

        const float sizeCorrection = 1.0 / (sqrt(samples) * 1.35914091423);
        const float apertureScale = sizeCorrection * aperture;

        const float inverseItter05 = 0.2 / samples;
        float lod = log2(abs(pcoc) * viewDimensions.x * viewDimensions.y * inverseItter05);

        vec2 distOffsetScale = apertureScale * vec2(1.0, aspectRatio);

        vec2 toCenter = screenCoord.xy - 0.5;
        vec2 prep = normalize(vec2(toCenter.y, -toCenter.x));
        float lToCenter = flength(toCenter);
        float angle = cos(lToCenter * DOF_DISTORTION_BARREL);

        for(int i = 0; i < samples; ++i) {
            r += 1.0 / r;

            pcocAngle = rot * pcocAngle;
            sampleAngle = rot * sampleAngle;

            vec2 pos = CalculateDistOffset(prep, 1.0, sampleAngle * r - sampleAngle) * sizeCorrection + 0.5;
            vec3 bokeh = texture2D(colortex0, pos * LENS_BUFFER_SCALE + LENS_BUFFER_OFFSET).rgb;

            pos = CalculateDistOffset(prep, angle, pcocAngle * r - pcocAngle) * distOffsetScale;

            dof += DecodeColour(texture2DLod(colortex3, screenCoord + pos, lod).rgb) * bokeh;
            weight += bokeh;            
        }

        return dof / weight;
    }

#endif
