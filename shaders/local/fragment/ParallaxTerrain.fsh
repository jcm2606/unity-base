/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_PARALLAX_TERRAIN
    #define LOCAL_INCL_FRAG_PARALLAX_TERRAIN

    #extension GL_ARB_shader_texture_lod : enable

#if defined PARALLAX_TERRAIN && defined gbuffers_terrain
    #define textureParallax(tex, coord) texture2DGradARB(tex, coord, texD[0], texD[1])
#else
    #define textureParallax(tex, coord) texture2D(tex, coord)
#endif

    #define tileSize     tileInfo[0]
    #define tilePosition tileInfo[1]

    vec2 WrapTextureCoords(vec2 uvCoord) {
        return ceil((tilePosition - uvCoord) / tileSize) * tileSize + uvCoord;
    }

    float GetDepthGradient(vec2 uvCoord, mat2 texD) {
        float parallaxDepth = tileSize.x * PARALLAX_TERRAIN_DEPTH;
        return textureParallax(normals, WrapTextureCoords(uvCoord)).a * parallaxDepth - parallaxDepth;
    }

    vec2 CalculateParallaxCoords(out vec2 parallaxCoord, vec2 uvCoord, mat2 texD, int blockID) {
        parallaxCoord = uvCoord;

    #ifndef PARALLAX_TERRAIN
        return uvCoord;
    #endif

        float texHeight = texture2D(normals, uvCoord).a;
        if(texHeight <= 0.0 || texHeight >= 1.0) return uvCoord;

        const float quality = 1e-6 / -1.0;

        vec3 direction = tangentDirectionView / flength(tangentDirectionView.xy);
             direction = flength(direction.xy * texD) * direction;

        vec3 coord = vec3(uvCoord, 0.0);

        while(GetDepthGradient(coord.xy, texD) <= coord.z && direction.z < quality) {
            coord += direction;
        }

        parallaxCoord = coord.xy;
        return WrapTextureCoords(coord.xy);
    }

    float CalculateParallaxShadow(const vec2 uvCoord, const mat2 texD, const int id) {
    #ifndef PARALLAX_TERRAIN_SHADOWS
        return 1.0;
    #endif

        float texHeight = texture2D(normals, uvCoord).a;
        if(texHeight <= 0.0 || texHeight >= 1.0) return 1.0;

        const float quality = 1e-6 / 1.0;

        vec3 increment = ((mat3(gbufferModelViewInverse) * shadowLightPosition) * tbn);
             increment = increment / flength(increment.xy);
             increment = flength(increment.xy * texD) * increment;

        vec3 coord = vec3(uvCoord, GetDepthGradient(uvCoord, texD));

        while(GetDepthGradient(coord.xy, texD) <= coord.z && increment.z > quality && coord.z < 0.0) {
            coord += increment;
        }

        return coord.z < 0.0 ? 0.0 : 1.0;
    }

#endif