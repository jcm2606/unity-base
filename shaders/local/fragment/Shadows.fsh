/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_SHADOWS
    #define LOCAL_INCL_FRAG_SHADOWS

    #include "/unity/shared/ShadowConversion.glsl"
    
    vec2 CalculateFilterRadius(vec3 shadowClipPosition, vec2 initialOffset, vec2 dither) {
        shadowClipPosition = shadowClipPosition * 0.5 + 0.5;

        const int   samples = SHADOW_SEARCH_SAMPLES;
        const float rSamples = 1.0 / samples;

        const mat2 rot = mat2(cos(GOLDEN), -sin(GOLDEN), sin(GOLDEN), cos(GOLDEN));

        float lightAngularRadius = sunAngle < 0.5 ? sunAngularRadius : moonAngularRadius;
        float lightSize = tan(lightAngularRadius) * shadowDepthBlocks;

        float searchRadius = 0.5 * shadowProjection[0].x;
        float searchLod = log2(2.0 * shadowResolution * searchRadius * inversesqrt(samples));
        float searchCmpMul = searchRadius / lightSize;

        vec2 avgDepth = vec2(0.0);
        vec2 validSamples = vec2(0.0);

        for(int i = 0; i < samples; ++i) {
            vec2 offset = initialOffset * sqrt((i + dither.y) * rSamples);
            initialOffset *= rot;

            vec2 coord = DistortShadowPositionProj(offset * searchRadius + shadowClipPosition.xy);

            vec2 depth = shadowClipPosition.z - vec2(texture2DLod(shadowtex1, coord, searchLod).x, texture2DLod(shadowtex0, coord, searchLod).x);
            vec2 valid = step(flength(offset) * searchCmpMul, depth);
            avgDepth += depth * valid;
            validSamples += valid;
        }

        avgDepth /= saturate(1.0 - validSamples) + validSamples;

        float spread = lightSize * shadowProjection[0].x;
        float maxSpread = 4.0 * shadowProjection[0].x;

        return min(vec2(maxSpread), avgDepth) * spread * 2.0;
    }

    vec3 CalculateDualPCF(Mask masks, vec3 shadowClipPosition, vec3 shadowViewPosition, vec3 shadowNormal, vec2 filterRadius, vec2 initialOffset, vec2 dither, float bias) {
        shadowClipPosition = shadowClipPosition * 0.5 + 0.5;

        const int   samples  = SHADOW_FILTER_SAMPLES;
        const float rSamples = 1.0 / samples;

        const mat2 rot = mat2(cos(GOLDEN), -sin(GOLDEN), sin(GOLDEN), cos(GOLDEN));

        vec3 shadowColour = vec3(0.0);

        for(int i = 0; i < samples; ++i) {
            vec2 offset = initialOffset * sqrt((i + dither.y) * rSamples);
            initialOffset *= rot;

            vec3 coordBack  = vec3(DistortShadowPositionProj(offset * filterRadius.x + shadowClipPosition.xy), shadowClipPosition.z);
            vec3 coordFront = vec3(DistortShadowPositionProj(offset * filterRadius.y + shadowClipPosition.xy), shadowClipPosition.z);

            vec4 sampleAux = texture2D(shadowcolor1, coordFront.xy);

            bool isWater = sampleAux.a * 2.0 - 1.0 > 0.5;

            float depthBack  = texture2DLod(shadowtex1, coordBack.xy, 0.0).x;
            float depthFront = texture2DLod(shadowtex0, coordFront.xy, 0.0).x;
            vec3 sampleNormal = sampleAux.xyz * 2.0 - 1.0;
            bias = (depthFront == depthBack && !(masks.foliage)) ? (dot(sampleNormal, shadowNormal) > 0.1 ? bias : 0.0) : bias;

            if(isWater) coordFront = vec3(DistortShadowPositionProj(offset * 1.1e-4 + shadowClipPosition.xy), shadowClipPosition.z);

            float shadowBack  = step(coordBack.z - bias,  depthBack);
            float shadowFront = step(coordFront.z - bias, depthFront);

            vec4 sampleColour = texture2D(shadowcolor0, coordFront.xy);
            sampleColour.rgb  = ToLinear(sampleColour.rgb * 4.0);
            if(!isWater) sampleColour.rgb = sampleColour.rgb * -sampleColour.a + sampleColour.rgb;

            shadowColour += mix(vec3(shadowBack), sampleColour.rgb, saturate(shadowBack - shadowFront));
        }

        return shadowColour * rSamples;
    }

    vec3 CalculateShadows(Mask masks, vec3 scenePosition, vec3 normal, vec2 dither) {
        dither.x = dither.x * dither.y + 0.5;
        dither.y = dither.x / dither.y;

        vec3 shadowViewPosition = transMAD(shadowModelView, scenePosition);
        vec3 shadowClipPosition = projMAD3(shadowProjection, shadowViewPosition);
        shadowClipPosition.z   *= shadowDepthMult;
        
        float NoL = saturate(dot(normal, shadowLightDirection));
              NoL = (masks.foliage) ? 0.5 : NoL;
        float bias = sqrt(sqrt(1.0 - NoL * NoL) / NoL) * rShadowResolution;
              bias = bias * CalculateShadowDistortionFactor(shadowClipPosition.xy) * 0.35;

        vec3 shadowNormal = mat3(shadowModelView) * normal;

        vec2 offset = sincos(dither.x * GOLDEN);

        vec2 filterRadius = CalculateFilterRadius(shadowClipPosition, offset, dither);

        return CalculateDualPCF(masks, shadowClipPosition, shadowViewPosition, shadowNormal, filterRadius, offset, dither, bias);
    }

#endif
