/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_WATER_WAVES
    #define LOCAL_INCL_FRAG_WATER_WAVES

    #include "/unity/shared/Noise.glsl"

    #define WaterHeightFunction Height0 // [Height0]

    float Gerstner(vec2 coord, vec2 direction, float steepness, float amplitude, float waveLength, float speed) {
        const float g = 19.6;

        float k = TAU / waveLength;
        float w = sqrt(g * k);
        float x = w * speed - (k * dot(direction, coord));
        float wave = sin(x) * 0.5 + 0.5;

        return amplitude * pow(wave, steepness);
    }
    
    float Height0(vec3 worldPosition) {
        const int octaves = 5;
        
        const float rotation = radians(67.5);
        const mat2 rotMatrix = crotate2(rotation);
        
        vec2 position = worldPosition.xz - worldPosition.y;
        vec2 noisePosition = position * 0.004;

        float speed = TIME * 0.3;
        const float steepness = 0.7;
        float amplitude = 0.07;
        float len = 2.25;
        vec2 direction = vec2(-0.7, 0.5);

        float height = 0.0;

        for(int i = 1; i <= octaves; ++i) {
            vec2 noise = noise2DSmooth(noisePosition / sqrt(len) + direction * (speed * -0.03));
            vec2 positionOffset = (noise * 2.0 - 1.0) * (sqrt(len) / max1(i * 0.45));

            height -= Gerstner(position + positionOffset, direction, steepness, amplitude, len, speed) * (noise.x * 0.5 + 0.5);

            amplitude /= i;
            len *= 0.65;
            speed *= -1.1;
            direction *= rotMatrix;
            noisePosition *= rotMatrix;
        }

        return height;
    }

    vec3 CalculateWaterNormal(vec3 worldPosition) {
        const float delta    = 0.2;
        const vec2  deltaPos = vec2(delta, 0.0);

        const float steepness = 1.0;

        float height0 = WaterHeightFunction(worldPosition);
        float height1 = WaterHeightFunction(worldPosition + deltaPos.xyy);
        float height2 = WaterHeightFunction(worldPosition + deltaPos.yyx);

        vec2 gradient = vec2(height0 - height1, height0 - height2);

        vec3 normal = normalize(vec3(gradient.x, gradient.y, 1.0 - gradient.x * gradient.x - gradient.y * gradient.y));
        normal = normal * steepness + vec3(0.0, 0.0, 1.0 - steepness);

        return normal;
    }

    vec3 CalculateWaterParallax(vec3 worldPosition, vec3 viewDirection) {
        const int steps = 4;
        const float rSteps = 1.0 / steps;

        const float height = 0.75;

        const float stepLength = rSteps * height * 4.0;

        viewDirection.xy *= stepLength / flength(viewDirection);

        for(int i = 0; i < steps; ++i) {
            worldPosition.xz = viewDirection.xy * abs(WaterHeightFunction(worldPosition)) + worldPosition.xz;
        }

        return worldPosition;
    }

#endif
