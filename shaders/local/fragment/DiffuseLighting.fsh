/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_DIFFUSE_LIGHTING
    #define LOCAL_INCL_FRAG_DIFFUSE_LIGHTING

    #include "/local/fragment/Shadows.fsh"

    // DIFFUSE OPERATORS
    float GeometrySchlickGGX(float NoV, float k) {
        return NoV / (NoV * -k + (NoV + k));
    }

    vec3 GeometrySmithGGX(vec3 diffuseColor, vec3 N, vec3 V, vec3 L, float r){
        float k = pow2(r + 1.0) * 0.125;
        float NoL = saturate(dot(N, L));

        float multiScattering = 0.1159 * r;

        return (diffuseColor * (multiScattering * NoL) + GeometrySchlickGGX(NoL, k)) * rPI;
    }

    vec3 DiffuseLambertian(vec3 diffuseColor, vec3 N, vec3 V, vec3 L, float r) {
        return vec3(saturate(dot(N, L)) * rPI);
    }

    // LIGHTMAP HELPERS
    float CalculateLightmapDistance(float lightmap) {
        lightmap = saturate(lightmap * 1.135);
        return -16.0 * lightmap + 17.0;
    }

    float CalculateLightmapInverseSquareAttenuation(float lightmap) {
        float dist = CalculateLightmapDistance(lightmap);
        return lightmap * pow(dist, -2.0);
    }

    // LIGHTING COMPONENTS
    vec3 CalculateDirectLight(Material material, vec3 sceneDirection, vec3 shadows) {
        vec3 shading = GeometrySmithGGX(material.albedo.rgb, material.normal, sceneDirection, shadowLightDirection, material.roughness);
        return lightColour * shadows * shading;
    }

    vec3 CalculateBlockLight(Material material) {
        const float brightness = float(BLOCK_LIGHT_BRIGHTNESS) * 0.25;
    #if   BLOCK_LIGHT_COLOURING_MODE == BLOCK_LIGHT_COLOURING_BLACKBODY
        const float temperature = float(BLOCK_LIGHT_TEMPERATURE);
        CFUNC_Blackbody(temperature, colour)
    #elif BLOCK_LIGHT_COLOURING_MODE == BLOCK_LIGHT_COLOURING_RGB
        const vec3 colour = vec3(BLOCK_LIGHT_RGB_R, BLOCK_LIGHT_RGB_G, BLOCK_LIGHT_RGB_B) / 255.0;
    #endif

        float attenuation = CalculateLightmapInverseSquareAttenuation(material.blockLight);
        float emission = ((material.emission > 0.0) ? material.emission : float(material.masks.emissive)) * 2.0;

        return colour * (brightness * max(attenuation, emission));
    }

    vec3 CalculateSkyLight(Material material) {
        float attenuation = pow3(material.skyLight);
        return FromSH(skySH[0], skySH[1], skySH[2], material.normal) * (attenuation * material.defaultAO * rPI * 1.46694220692);
    }

    vec3 CalculateSSS(Material material, vec3 sceneDirection, vec3 shadows) {
        float foliageMask = float(material.masks.foliage) * 0.5;
        return lightColour * sqrt(material.albedo.rgb) * shadows * foliageMask;
    }

    vec3 CalculateDiffuseLighting(Material material, vec3 scenePosition, vec2 dither, out vec3 shadowPassthrough) {
        vec3 sceneDirection = normalize(scenePosition);

        vec3 directLightShadows = CalculateShadows(material.masks, scenePosition, material.normal, dither) * material.parallaxShadow;
        shadowPassthrough = directLightShadows;

        vec3 direct = CalculateDirectLight(material, sceneDirection, directLightShadows);
        vec3 ambient = CalculateSkyLight(material);
        vec3 block = CalculateBlockLight(material);
        vec3 sss = CalculateSSS(material, sceneDirection, directLightShadows);

        return material.albedo.rgb * (direct + ambient + block + sss);
    }

#endif
