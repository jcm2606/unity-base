/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_SPECULAR_LIGHTING
    #define LOCAL_INCL_FRAG_SPECULAR_LIGHTING

    // RAYTRACER
    vec3 Raytrace(vec3 viewPosition, vec3 screenPosition, vec3 reflectedDirection, vec3 sky, float skyFalloff, vec2 dither, float NoV, float roughness) {
    #ifdef TAA
        screenPosition.xy -= taaJitter * 0.5;
    #endif

        const int   steps  = 8;
        const float rSteps = 1.0 / steps;

        int refinements = 4;

        vec3 direction = normalize(ViewToScreenPosition(viewPosition + reflectedDirection) - screenPosition);

        const float maxLength = rSteps;
        const float minLength = maxLength * 0.01;

        float stepLength = mix(minLength, maxLength, NoV) * (dither.x + 1.0);
        float stepWeight = 1.0 / abs(direction.z);

        screenPosition += direction * stepLength;

        float depth = texture2D(depthtex1, screenPosition.xy).x;

        for(int i = 0; i < steps; ++i) {
            stepLength = clamp((depth - screenPosition.z) * stepWeight, minLength, maxLength);
            screenPosition += direction * stepLength;

            if(saturate(screenPosition) != screenPosition) return sky * skyFalloff; // Ray has gone off screen.

            depth = texture2D(depthtex1, screenPosition.xy).x;

            if(depth <= screenPosition.z) break;
        }

        if(depth >= 1.0) return sky; // Ray has landed directly on the sky.

        while(--refinements > 0) {
            vec3 refPosition = direction * clamp((depth - screenPosition.z) * stepWeight, -stepLength, stepLength);
            float refDepth = texture2D(depthtex1, refPosition.xy).x;

            if(refDepth < refPosition.z) {
                screenPosition = refPosition;
                depth = refDepth;
                break;
            }

            stepLength *= 0.5;
        }

        bool visible = abs(screenPosition.z - depth) * min(stepWeight, 400.0) <= maxLength && 0.96 < depth;

        return visible ? DecodeRGBE8(texture2D(colortex4, screenPosition.xy)) : sky * skyFalloff;
    }

    // SPECULAR OPERATORS
    float GSpecular(float alpha2, float NoV, float NoL) {
        float x = 2.0 * NoL * NoV;
        float y = (1.0 - alpha2);
        return x / (NoV * sqrt(alpha2 + y * (NoL * NoL)) + NoL * sqrt(alpha2 + y * (NoV * NoV)));
    }

    vec2 G1G2SmithGGX(float alpha2, float NoV, float NoL) {
        vec2 delta = vec2(NoV, NoL);
             delta = abs(sqrt((delta - delta * alpha2) * delta + alpha2) / delta);
        vec2 denominator = vec2(delta.x + 1.0, delta.x + delta.y);
        return saturate(2.0 / denominator);
    }

    float GGXDistribution(const float alpha2, const float NoH) {
        float d = (NoH * alpha2 - NoH) * NoH + 1.0;
        return alpha2 / (PI * d * d);
    }

    float SchlickFresnel(float f0, float f90, float LoH) {
        //return (f90 - f0) * pow5(1. - LoH) + f0;
        return (f90 - f0) * exp2((-5.55473 * LoH - 6.98316) * LoH) + f0;
    }

    vec3 ExactFresnel(const vec3 n, const vec3 k, float c) {
        const vec3 k2= k * k;
        const vec3 n2k2 = n * n + k2;

        vec3 c2n = (c * 2.0) * n;
        vec3 c2 = vec3(c * c);

        vec3 rs_num = n2k2 - c2n + c2;
        vec3 rs_den = n2k2 + c2n + c2;

        vec3 rs = rs_num / rs_den;

        vec3 rp_num = n2k2 * c2 - c2n + 1.0;
        vec3 rp_den = n2k2 * c2 + c2n + 1.0;

        vec3 rp = rp_num / rp_den;

        return saturate(0.5 * (rs + rp));
    }

    vec3 Fresnel(float f0, float f90, float LoH) {
        /*
        if(f0 > 0.985) {
            const vec3 chromeIOR = vec3(3.1800, 3.1812, 2.3230);
            const vec3 chromeK = vec3(3.3000, 3.3291, 3.1350);
            return ExactFresnel(chromeIOR, chromeK, LoH);
        } else if(f0 > 0.965) {
            const vec3 goldIOR = vec3(0.18299, 0.42108, 1.3734);
            const vec3 goldK = vec3(3.4242, 2.3459, 1.7704);
            return ExactFresnel(goldIOR, goldK, LoH);
        } else if(f0 > 0.45) {
            const vec3 ironIOR = vec3(2.9114, 2.9497, 2.5845);
            const vec3 ironK = vec3(3.0893, 2.9318, 2.7670);
            return ExactFresnel(ironIOR, ironK, LoH);
        } else { */
            return vec3(SchlickFresnel(f0, f90, LoH));
        //}
    }

    vec3 SampleDistribution(float p, float alpha2, int steps) {
        float x = (alpha2 * p) / (1.0 - p);
        float y = p * float(steps) * 4096.0 * GOLDEN;

        float c = inversesqrt(x + 1.0);
        float s = sqrt(x) * c;

        return vec3(cos(y) * s, sin(y) * s, c);
    }

    vec3 BlendMaterial(vec3 Kdiff, vec3 Kspec, vec3 albedo, float f0) {
        if(f0 < 0.004) return Kdiff;

        float scRange = smoothstep(0.25, 0.45, f0);
        vec3 dielectric = Kdiff + Kspec;
        vec3 metal = albedo * Kspec;

        return mix(dielectric, metal, scRange);
    }

    vec3 GGX(vec3 N, vec3 V, float f0, float a2) {
        vec3 H = normalize(shadowLightDirectionView + V);

        float VoH = saturate(dot(V, H));
        float NoL = saturate(dot(N, shadowLightDirectionView));
        float NoV = saturate(dot(N, V));
        float VoL = (dot(V, shadowLightDirectionView));
        float NoH = saturate(dot(N, H));

        float D = GGXDistribution(a2, NoH);
        float G = GSpecular(a2, NoV, NoL);
        vec3 F = Fresnel(f0, 1.0, VoH);

        return max0(F * D * G / (4.0 * NoL * NoV)) * NoL;
    }

    vec3 SharpHighlight(vec3 N, vec3 V, float f0) {
        vec3 R = reflect(-V, N);
        
        float LoR = (dot(sunDirectionView, R));

        float NoV = saturate(dot(N, V));
        vec3 F = Fresnel(f0, 1.0, NoV);

        vec3 sunSpot  = CalculateSunSpot(  LoR) * skySunColor;
        vec3 moonSpot = CalculateMoonSpot(-LoR) * skyMoonColor;

        return (sunSpot + moonSpot) * F;
    }

    // SPECULAR LIGHTING FUNCTIONS
    vec3 CalculateSpecularHighlight(vec3 viewNormal, vec3 viewDirection, float f0, float roughness, float a2) {
        if(roughness >= 0.03) {
            return lightColour * GGX(viewNormal, viewDirection, f0, a2);
        } else {
            return SharpHighlight(viewNormal, viewDirection, f0);
        }
    }

    vec3 CalculateRaytracedReflections(vec3 viewPosition, vec3 screenPosition, vec3 viewDirection, vec3 normal, vec3 viewNormal, float skyLight, float f0, float roughness, float a2, vec2 dither) {
        const int   samples  = 2;
        const float rSamples = 1.0 / samples;

        float NoV = (dot(viewDirection, viewNormal));

        float skyFalloff = pow6(saturate(skyLight * 1.1));
        
        vec3 tangent = normalize(cross(gbufferModelView[1].xyz, viewNormal));
        mat3 tbn = mat3(tangent, cross(tangent, viewNormal), viewNormal);

        float offset = dither.x * rSamples;

        vec3 reflection = vec3(0.0);

        for(int i = 0; i < samples; ++i) {
            vec3 H = (tbn * SampleDistribution((offset + float(i)) * rSamples, a2, samples));
            vec3 L = reflect(-viewDirection, H);

            float VoH = saturate(dot(viewDirection, H));
            float NoL = saturate(dot(viewNormal, L));

            vec3 worldL = mat3(gbufferModelViewInverse) * L;
            vec3 sky = DecodeRGBE8(texture2D(colortex6, UnprojectSphere(worldL) * skyDomeQuality));

            vec3 extinction = Fresnel(f0, 1.0, VoH) * GSpecular(a2, NoV, NoL);

            if(roughness < 0.99) { // Only raytrace the reflections if the roughness is below a certain value.
                reflection += Raytrace(viewPosition, screenPosition, L, sky, skyFalloff, dither, NoV, roughness) * extinction;
            } else {
                reflection += (sky * skyFalloff) * extinction;
            }
        }

        return reflection * rSamples;
    }

    vec3 ClampNormal(vec3 normal) {
        // For some reason the GGX distribution doesn't like it when the normal is 1.0 in the Y axis (up/down).
        // This function clamps the normal to prevent the distribution from breaking.
        // Hacky solution, would highly advise anyone else to actually try to fix it, but I can't be bothered, and this doesn't break much else.
        const float clampAccuracy = 1e-9;
        const float clampCeiling  = 1.0 / (1.0 + clampAccuracy);
        normal.y = min(normal.y, clampCeiling);
        return normal;
    }

    vec3 CalculateSpecularLighting(Position position, Material material, vec3 diffuse, vec3 shadows, vec2 dither) {
        //float a  = material.roughness * material.roughness;
        //float a2 = a * a;
        //material.roughness = ceil(screenCoord.x * 4.0) / 5.0;
        float a2 = material.roughness * material.roughness * material.roughness * material.roughness;

        vec3 viewNormal = mat3(gbufferModelView) * material.normal;//ClampNormal(material.normal);
        vec3 viewDirection = normalize(-position.viewPosition);

        vec3 specular  = CalculateRaytracedReflections(position.viewPosition, vec3(screenCoord, position.depth), viewDirection, material.normal, viewNormal, material.skyLight, material.reflectance, material.roughness, a2, dither);
             specular += CalculateSpecularHighlight(viewNormal, viewDirection, material.reflectance, material.roughness, a2) * shadows;

        return BlendMaterial(diffuse, specular, material.albedo.rgb, material.reflectance);
    }

#endif
