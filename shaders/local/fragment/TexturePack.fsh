/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_FRAG_TEXTURE_PACK
    #define LOCAL_INCL_FRAG_TEXTURE_PACK
    
    void ReadSpecularMap(vec4 specularData, ivec2 blockID, out float smoothness, out float reflectance, out float emission) {
        bool isSpecularMapPresent = dot(specularData, specularData) > 0.0;

        if(isSpecularMapPresent) { // If the texture pack has a specular map, read data from the map.
            #if   TEXTURE_PACK_FORMAT == SPECULAR
                smoothness = specularData.x;
                reflectance = 0.02;
                emission = 0.0;
            #elif TEXTURE_PACK_FORMAT == PBR_OLD
                smoothness = specularData.x;
                reflectance = mix(0.02, 0.8, specularData.y);
                emission = specularData.z;
            #elif TEXTURE_PACK_FORMAT == PBR_CONTINUUM
                smoothness = specularData.z;
                reflectance = specularData.x;
                emission = specularData.w;
            #elif TEXTURE_PACK_FORMAT == PBR_LAB
                smoothness = sqrt(specularData.x);
                reflectance = specularData.y * specularData.y;
                emission = specularData.w;
            #endif
        } else { // Otherwise default to hardcoded values for certain blocks.
            reflectance = 0.02; // Default reflectance to 0.02 for dielectrics.

            switch(blockID.x) {
                case 10012: // Water.
                            smoothness = 0.97; reflectance = 0.021; break;
                case 10017: // Iron.
                            smoothness = 0.5; reflectance = 0.8; break;
                case 10018: // Gold.
                            smoothness = 0.91; reflectance = 0.8; break;
                case 10019: // Diamond.
                            smoothness = 0.88; break;
                case 10021: // Obsidian.
                            smoothness = 0.55; break;
                case 10015: // Cutout Glass.
                            smoothness = 0.97; break;
                case 10016: // Stained Glass.
                            smoothness = 0.94; break;
                default: break;
            }
        }

        smoothness = 1.0 - smoothness; // Invert smoothness to roughness.
    #ifdef TEXTURE_PACK_SQR_ROUGHNESS
        smoothness = pow2(smoothness);
    #endif
    }

#endif
