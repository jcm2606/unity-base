/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_SETTINGS
    #define LOCAL_INCL_SETTINGS

    #define GLOBAL_TIME_MULT 1.0 // [0.0 0.083333 0.090909 0.1 0.111111 0.125 0.142857 0.166667 0.2 0.25 0.333333 0.5 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0 11.0 12.0]

    #include "/local/settings/TextureSettings.glsl"
    #include "/local/settings/TAASettings.glsl"
    #include "/local/settings/CameraSettings.glsl"
    #include "/local/settings/ToningSettings.glsl"
    #include "/local/settings/SkySettings.glsl"
    #include "/local/settings/LightingSettings.glsl"
    #include "/local/settings/ParallaxSettings.glsl"

#endif
