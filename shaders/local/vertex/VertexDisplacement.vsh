/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_VERT_VERTEX_DISPLACEMENT
    #define LOCAL_INCL_VERT_VERTEX_DISPLACEMENT

    #include "/local/vertex/WindDisplacement.vsh"

    vec3 CalculateVertexDisplacement(vec3 worldPosition, ivec2 blockID) {
        switch(blockID.x) {
            case 10000: // Single Ground Foliage.
                    worldPosition += CalculateWindTop(worldPosition, 1.0, 0.16, false, false); break;
            case 10005: // Crops.
                    worldPosition += CalculateWindTop(worldPosition, 1.0, 0.11, false, false); break;
            case 10001: // Double Ground Foliage, Bottom.
            case 10002: // Double Ground Foliage, Top.
                    worldPosition += CalculateWindTop(worldPosition, 1.0, 0.12, true, blockID.x == 10002); break;
            case 10004: // Sapling.
                    worldPosition += CalculateWindTop(worldPosition, 1.0, 0.16, false, false); break;
            case 10006: // Leaves.
                    worldPosition += CalculateWindFull(worldPosition, 1.0, 0.05); break;
            case 10007: // Vines.
                    worldPosition += CalculateWindFull(worldPosition, 1.0, 0.05); break;
            default: break;
        }

        return worldPosition;
    }

#endif
