/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined LOCAL_INCL_VERT_WIND_DISPLACEMENT
    #define LOCAL_INCL_VERT_WIND_DISPLACEMENT

    vec3 CalculateWindTop(vec3 worldPosition, float speed, float magnitude, bool isDoubleTall, bool isTopBlock) {
        speed *= TIME;

        bool isTopVertex = uvCoord.y < mc_midTexCoord.y;

        if(isDoubleTall) {
            magnitude *= mix(float(isTopVertex) * 0.5, float(isTopVertex) * 0.5 + 0.5, float(isTopBlock));
        } else {
            magnitude *= float(isTopVertex);
        }

        vec3 windVolume = vec3(0.0);
        windVolume.x = (cos(worldPosition.x * 4.0 + speed * 4.0) * 0.5 + 0.5) * 0.5 - cos(worldPosition.z * 2.0 + speed * 3.0)  * 0.5;
        windVolume.z =  cos(worldPosition.z * 1.5 - speed * 2.0) * 0.5 - (sin(worldPosition.z * 6.0 - speed * 2.5) * 0.5 + 0.5) * 0.5;

        return windVolume * magnitude;
    }

    vec3 CalculateWindFull(vec3 worldPosition, float speed, float magnitude) {
        speed *= TIME;

        vec3 windVolume = vec3(0.0);
        windVolume.x = (cos(worldPosition.x * 2.0 + speed * 4.0) * 0.5 + 0.5) * 0.5 - cos(worldPosition.y + speed * 2.0) * 0.5;
        windVolume.y = (cos(worldPosition.x * 4.0 - speed * 3.0) * 0.5 + 0.5) * 0.8 - sin(worldPosition.z * 2.0 + speed * 2.0) * 0.5;
        windVolume.z =  cos(worldPosition.y * 3.0 - speed) * 0.5 - (sin(worldPosition.z * 3.0 - speed * 2.5) * 0.5 + 0.5) * 0.5;

        return windVolume * magnitude;
    }

#endif
