/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#version 410 compatibility

#define composite15
#define fsh
#define dim0

#include "/program/composite15.glsl"
