/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_TYPE_AUTO
    #define UNITY_INCL_UTILITY_TYPE_AUTO

    #define rcp(x) ( 1.0 / (x) )

    #define max0_DEF(type) type max0(const type x) { return max(type(0.0), x); }
    DEFINE_genFType(max0_DEF)

    #define max1_DEF(type) type max1(const type x) { return max(type(1.0), x); }
    DEFINE_genFType(max1_DEF)

    #define min0_DEF(type) type min0(const type x) { return min(type(0.0), x); }
    DEFINE_genFType(min0_DEF)

    #define min1_DEF(type) type min1(const type x) { return min(type(1.0), x); }
    DEFINE_genFType(min1_DEF)

    #define saturate_DEF(type) type saturate(const type x) { return clamp(x, type(0.0), type(1.0)); }
    DEFINE_genFType(saturate_DEF)

    #define fsign(x) (saturate(x * 1e35) * 2.0 - 1.0)
    #define fstep(x,y) saturate((y - x) * 1e35)

    #define pow2_DEF(type) type pow2(const type x) { return x * x; }
    DEFINE_genFType(pow2_DEF)
    DEFINE_genIType(pow2_DEF)

    #define pow3_DEF(type) type pow3(const type x) { return pow2(x) * x; }
    DEFINE_genFType(pow3_DEF)
    DEFINE_genIType(pow3_DEF)

    #define pow4_DEF(type) type pow4(const type x) { return pow2(pow2(x)); }
    DEFINE_genFType(pow4_DEF)
    DEFINE_genIType(pow4_DEF)

    #define pow5_DEF(type) type pow5(const type x) { return pow2(pow2(x)) * x; }
    DEFINE_genFType(pow5_DEF)
    DEFINE_genIType(pow5_DEF)

    #define pow6_DEF(type) type pow6(const type x) { return pow2(pow2(x) * x); }
    DEFINE_genFType(pow6_DEF)
    DEFINE_genIType(pow6_DEF)

    #define pow7_DEF(type) type pow7(const type x) { return pow2(pow2(x) * x) * x; }
    DEFINE_genFType(pow7_DEF)
    DEFINE_genIType(pow7_DEF)

    #define pow8_DEF(type) type pow8(const type x) { return pow2(pow2(pow2(x))); }
    DEFINE_genFType(pow8_DEF)
    DEFINE_genIType(pow8_DEF)

#endif
