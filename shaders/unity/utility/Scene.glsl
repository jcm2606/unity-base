/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_SCENE
    #define UNITY_INCL_UTILITY_SCENE

    #define isLand(x) ( x < 1.0 - near / far / far )

    #define depthLinear(x) ( (2.0 * near) / (far + near - x * (far - near)) )
    #define depthExp(x) ( (far * (x - near)) / (x * (far - near)) )

    #define worldDailyCycle() ( (float(moonPhase) * 24000.0 + float(worldTime)) * 0.00000595238095238 )

#endif
