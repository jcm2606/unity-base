/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_VOLUME
    #define UNITY_INCL_UTILITY_VOLUME

    vec3 TransmittedScatteringIntegral(const float opticalDepth, const vec3 coeff) {
        const_arg  vec3 a = -coeff / log(2.0);
        const_arg  vec3 b = -1.0 / coeff;
        const_arg  vec3 c =  1.0 / coeff;

        return exp2(a * opticalDepth) * b + c;
    }

    float TransmittedScatteringIntegral(const float opticalDepth, const float coeff) {
        const_arg  float a = -coeff / log(2.0);
        const_arg  float b = -1.0 / coeff;
        const_arg  float c =  1.0 / coeff;

        return exp2(a * opticalDepth) * b + c;
    }

    float PhaseG(float theta, const float G) {
        const_arg float G2 = G * G;
        const_arg float p1 = (0.75 * (1.0 - G2)) / (TAU * (2.0 + G2));

        const_arg float G2_p2 = 1.0 + G2;
        const_arg float G_p2  = 2.0 * G;

        float p2 = (theta * theta + 1.0) * pow(G2_p2 - G_p2 * theta, -1.5);
        return p1 * p2;
    }
    #define PhaseG0() ( 0.25 )

    const float c = 0.7;
    const float mixer = 0.8;

    float PhaseG8(float x) {
	    const float g = 0.8 * c;
        const float g2 = g * g;
        const float g3 = log2((g2 * -0.25 + 0.25) * mixer);
        const float g4 = 1.0 + g2;
        const float g5 = -2.0 * g;

        return exp2(log2(g5 * x + g4) * -1.5 + g3);
    }

    float PhaseGM5(float x) {
        const float g = -0.5 * c;
        const float g2 = g * g;
        const float g3 = log2((g2 * -0.25 + 0.25) * (1.0 - mixer));
        const float g4 = 1.0 + g2;
        const float g5 = -2.0 * g;

        return exp2(log2(g5 * x + g4) * -1.5 + g3);
    }

    float Phase2Lobes(float x) {
        return PhaseG8(x) + PhaseGM5(x);
    }

#endif
