/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_TYPE_FLOAT
    #define UNITY_INCL_UTILITY_TYPE_FLOAT

    #define max3f(a, b, c) ( max(a, max(b, c)) )
    #define max4f(a, b, c, d) ( max(a, max(b, max(c, d))) )

    #define min3f(a, b, c) ( min(a, min(b, c)) )
    #define min4f(a, b, c, d) ( min(a, min(b, min(c, d))) )

#endif
