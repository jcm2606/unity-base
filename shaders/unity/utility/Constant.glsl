/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_CONSTANT
    #define UNITY_INCL_UTILITY_CONSTANT

    // AMD supposedly doesn't have a constant radians function, so I've implemented it manually using a macro.
    // radians should have a hardware optimised implementation, so this is not for use in dynamic expressions, ONLY constant expressions.
    #define cradians(x) ( (PI * x) / 180.0 )

    // Older NVIDIA drivers don't like using `mix()` in constant expressions.
    #define cmix(x, y, a) ( y * a + (x * -a + x) )

#endif
