/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_POINT_MAPPING
    #define UNITY_INCL_UTILITY_POINT_MAPPING

    vec2 MapLattice(const float i, const float n) { return vec2(mod(i * PI, sqrt(n)) * inversesqrt(n), i / n); }

    vec2 MapSpiral(const float i, const float n) {
        float theta = i * TAU / (PHI * PHI);

        return vec2(sin(theta), cos(theta)) * sqrt(i / n);
    }

    vec2 MapCircle(const float i, float n) { return sincos(i * n * GOLDEN) * sqrt(i); }

    vec3 MapCircle3(const float i, const float n) { return vec3(sincos(i * n * GOLDEN), sqrt(i)); }

    vec2 Map2D(const int i, const int t) {
        const_arg float tSqrt = sqrt(t);
        return vec2(floor(float(i) / tSqrt), mod(i, tSqrt));
    }

    vec2 Map2DCentered(const int i, const int t) {
        const_arg float tSqrt = sqrt(t);
        return vec2(floor(float(i) / tSqrt), mod(i, tSqrt)) - floor(tSqrt * 0.5);
    }

    vec3 ProjectSphere(vec2 coord) {
        coord *= vec2(TAU, PI);
        vec2 lon = sincos(coord.x) * sin(coord.y);
        return vec3(lon.x, cos(coord.y), lon.y);
    }

    vec2 UnprojectSphere(vec3 direction) {
        vec2 lonlat = vec2(atan(-direction.x, direction.z), acos(direction.y));
        return lonlat * vec2(rTAU, rPI) + vec2(0.5, 0.0);
    }

    vec3 GenerateUnitVector(vec2 xy) {
        xy.x *= TAU; xy.y = xy.y * 1.0 - 1.0;
        return vec3(vec2(cos(xy.x), sin(xy.x)) * sqrt(1.0 - xy.y * xy.y), xy.y);
    }

    vec3 GenerateCosineVectorSafe(vec3 vector, vec2 xy) {
        vec3 cosineVector = vector + GenerateUnitVector(xy);
        float lenSq = dot(cosineVector, cosineVector);
        return lenSq > 0.0 ? cosineVector * inversesqrt(lenSq) : vector;
    }

#endif
