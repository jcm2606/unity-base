/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_COMPAT
    #define UNITY_INCL_UTILITY_COMPAT

    /*
        Any vendor-specific quirks that can be dealt with via the preprocessor is done in this file.
        Here are the vendor defines:
            - MC_GL_VENDOR_ATI    = AMD
            - MC_GL_VENDOR_INTEL  = Intel
            - MC_GL_VENDOR_NVIDIA = NVIDIA
    */

    /*** Const Argument Support ***/
    #if defined MC_GL_VENDOR_ATI || defined MC_GL_VENDOR_INTEL
        // If using a troublesome vendor, use...
        #define const_arg
    #else
        // Else, use...
        #define const_arg const
    #endif

#endif
