/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_COLOR
    #define UNITY_INCL_UTILITY_COLOR

    const float gammaCurveScreen    = 2.2;
    const float gammaCurveScreenRCP = 1.0 / gammaCurveScreen;

    #define toGamma_DEF(type) type ToGamma(const type x) { return pow(x, type(gammaCurveScreenRCP)); }
    DEFINE_genFType(toGamma_DEF)

    #define toLinear_DEF(type) type ToLinear(const type x) { return pow(x, type(gammaCurveScreen)); }
    DEFINE_genFType(toLinear_DEF)

    const vec3 lumaCoefficient = vec3(0.2125, 0.7154, 0.0721);
    #define luma(x) ( dot(x, lumaCoefficient) )

    vec3 SaturationMod(const vec3 x, const float s) { return mix(vec3(dot(x, lumaCoefficient)), x, s); }
    #define saturationMod(x, s) ( cMix(vec3(dot(x, lumaCoefficient)), x, s) )

    vec3 Blackbody(const float K) {
        const vec4 vx = vec4(-0.2661239e9, -0.2343580e6, 0.8776956e3, 0.179910);
        const vec4 vy = vec4(-1.1063814, -1.34811020, 2.18555832, -0.20219683);

        const mat3 xyzToSrgb = mat3(
             3.2404542, -1.5371385, -0.4985314,
            -0.9692660,  1.8760108,  0.0415560,
             0.0556434, -0.2040259,  1.0572252
        );

        const_arg float kRCP  = rcp(K);
        const_arg float kRCP2 = kRCP * kRCP;

        const_arg float x  = dot(vx, vec4(kRCP * kRCP2, kRCP2, kRCP, 1.0));
        const_arg float x2 = x * x;
        const_arg float y  = dot(vy, vec4(x * x2, x2, x, 1.0));
        const_arg float z  = 1.0 - x - y;

        return max0(vec3(x / y, 1.0, z / y) * xyzToSrgb);
    }

    #define CFUNC_Blackbody(t, var) \
        const vec2  cfunc_blkbdy_K  = vec2(t, 1.0 / t); \
        const float cfunc_blkbdy_x  = dot(vec4(-0.2661239e9, -0.2343580e6, 0.8776956e3, 0.179910), vec4(cfunc_blkbdy_K.y * cfunc_blkbdy_K.y * cfunc_blkbdy_K.y, cfunc_blkbdy_K.y * cfunc_blkbdy_K.y, cfunc_blkbdy_K.y, 1.0)); \
        const float cfunc_blkbdy_y  = dot(vec4(-1.1063814, -1.34811020, 2.18555832, -0.20219683), vec4(cfunc_blkbdy_x * cfunc_blkbdy_x * cfunc_blkbdy_x, cfunc_blkbdy_x * cfunc_blkbdy_x, cfunc_blkbdy_x, 1.0)); \
        const float cfunc_blkbdy_z  = 1.0 - cfunc_blkbdy_x - cfunc_blkbdy_y; \
        const vec3 var = max(vec3(0.0), vec3(cfunc_blkbdy_x / cfunc_blkbdy_y, 1.0, cfunc_blkbdy_z / cfunc_blkbdy_y) * mat3(3.2404542, -1.5371385, -0.4985314, -0.9692660,  1.8760108,  0.0415560, 0.0556434, -0.2040259,  1.0572252)); \

#endif
