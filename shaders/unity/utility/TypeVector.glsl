/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_TYPE_VECTOR
    #define UNITY_INCL_UTILITY_TYPE_VECTOR

    float maxof(vec2 v) { return max(v.x, v.y); }
    float maxof(vec3 v) { return max(max(v.x, v.y), v.z); }
    float maxof(vec4 v) { return max(max(max(v.x, v.y), v.z), v.w); }

    float minof(vec2 v) { return min(v.x, v.y); }
    float minof(vec3 v) { return min(min(v.x, v.y), v.z); }
    float minof(vec4 v) { return min(min(min(v.x, v.y), v.z), v.w); }

    float sumof(vec2 v) { return v.x + v.y; }
    float sumof(vec3 v) { return v.x + v.y + v.z; }
    float sumof(vec4 v) { return v.x + v.y + v.z + v.w; }

    #define flengthsqr_DEF(type) float flengthsqr(type x) { return dot(x, x); }
    DEFINE_genVType(flengthsqr_DEF)

    #define flength_DEF(type) float flength(type x) { return sqrt(dot(x, x)); }
    DEFINE_genVType(flength_DEF)

    #define fdistance_DEF(type) float fdistance(type a, type b) { return flength(b - a); }
    DEFINE_genVType(fdistance_DEF)

    vec2 sincos(const float x) { return vec2(sin(x), cos(x)); }

    vec2 rotate2(const vec2 v, const float r) {
        const_arg vec2 sinCos = vec2(sin(r), cos(r));
        return v * mat2(sinCos.x, -sinCos.y, sinCos.y, sinCos.x);
    }
    #define crotate2(r) mat2(sin(r), -cos(r), cos(r), sin(r))

    mat3 RotateMatrix(vec3 x, vec3 y) {
        float d = dot(x, y);
        float id = 1.0 - d;

        vec3 cr = cross(y, x);
        float s = length(cr);

        vec3 m = cr / s;
        vec3 m2 = m * m * id + d;

        vec3 sm = s * m;
        vec3 w = (m.xy * id).xxy * m.yzz;

        return mat3(
            m2.x      , w.x - sm.z, w.y + sm.y,
            w.x + sm.z, m2.y      , w.z - sm.x,
            w.y - sm.y, w.z + sm.x, m2.z
        );
    }

#endif
