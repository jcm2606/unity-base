/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY_ENCODING
    #define UNITY_INCL_UTILITY_ENCODING

    float Pack2x4(vec2 x) {
        return dot(floor(15.0 * x + 0.5), vec2(1.0 / 255.0, 16.0 / 255.0));
    }

    vec2 Unpack2x4(float pack) {
        pack *= 255.0 / 16.0;
        vec2 xy; xy.y = floor(pack); xy.x = pack - xy.y;
        return vec2(16.0 / 15.0, 1.0 / 15.0) * xy;
    }

    float Encode2x8(vec2 decoded) {
        const vec2 a = vec2(1.0, 256.0) / 65535.0;
        return dot(floor(decoded * 255.0), a);
    }
    
    vec2 Decode2x8(float encoded) {
        const vec2  a = 65535.0 / vec2(256.0, 65536.0);
        const float b = 256.0 / 255.0;
        return fract(encoded * a) * b;
    }

    float Encode3x8(vec3 decoded) {
        const vec3 m = vec3(31.0, 63.0, 31.0);
        const float a = 1.0 / 65535.0;
        decoded = saturate(decoded);
        ivec3 b = ivec3(decoded * m);
        return float(b.r | (b.g << 5) | (b.b << 11) ) * a;
    }

    vec3 Decode3x8(float encoded) {
        const vec3 m = 1.0 / vec3(31.0, 63.0, 31.0);
        int bf = int(encoded * 65535.0);
        return vec3(mod(bf, 32), mod(bf >> 5, 64), bf >> 11) * m;
    }

    float Encode2x16F(const vec2 decoded) {
        uvec2 v = uvec2(round(saturate(decoded) * 65535.0)) << uvec2(0, 16);

        return uintBitsToFloat(v.x + v.y);
    }

    vec2 Decode2x16F(const float encoded) {
        uvec2 decoded   = uvec2(floatBitsToUint(encoded));
              decoded.y = decoded.y >> 16;
              decoded.x = decoded.x & 65535;

        return vec2(decoded) / 65535.0;
    }

    float Encode4x8F(vec4 decoded) {
        const vec3  cExponent1 = exp2(vec3(0.0, 8.0, 16.0));
        const float cExponent2 = exp2(-24.0);

        decoded = saturate(decoded);
        decoded = round(decoded * vec4(254.0, 255.0, 255.0, 252.0));
        
        float z_sign = (decoded.b < 128.0 ? 1.0 : -1.0);
        decoded.b = mod(decoded.b, 128.0);
        
        float encode = dot(decoded.rgb, cExponent1);
        
        return ldexp((encode * cExponent2 + 0.5) * z_sign, int(decoded.a - 125.0));
    }

    vec4 Decode4x8F(const float encoded) {
        const float cExponent1 = exp2(24.0);
        const vec3  cExponent2 = exp2(vec3(8.0, 16.0, 24.0));
        const vec3  cExponent3 = exp2(-vec3(0.0, 8.0, 16.0));
        const vec4  divisor    = rcp(vec4(254.0, 255.0, 255.0, 252.0));

        int exponent = 0;
        float packedFloat = (frexp(encoded, exponent) - 0.5) * cExponent1;
        
        float z_sign2 = sign(packedFloat);
        packedFloat *= z_sign2;
        
        vec4 decoded = vec4(0.0);
        decoded.xyz  = mod(vec3(packedFloat), cExponent2);
        decoded.yz  -= decoded.xy;
        decoded.xyz *= cExponent3;
        decoded.w    = float(exponent) + 125.0;
        
        if (z_sign2 < 0.0) decoded.z += 128.0;
        
        return decoded * divisor;
    }

    float EncodeNormal(vec3 normal) {
        const float bits = 11.0;
        const float a    = exp2(bits);
        const float b    = exp2(bits + 2.0);
               normal    = clamp(normal, -1.0, 1.0);
               normal.xy = vec2(atan(normal.x, normal.z), acos(normal.y)) * rPI;
               normal.x += 1.0;
               normal.xy = round(normal.xy * a);
        return normal.y * b + normal.x;
    }

    vec3 DecodeNormal(const float encoded) {
        const float bits = 11.0;
        const float a = exp2(bits + 2.0);
        const float b = 1.0 / a;
        const vec2  c = 1.0 / exp2(vec2(bits, bits * 2.0 + 2.0));
        vec4 normal = vec4(0.0);
             normal.y    = a * floor(encoded * b);
             normal.x    = encoded - normal.y;
             normal.xy  *= c;
             normal.x   -= 1.0;
             normal.xy  *= PI;
             normal.xwzy = vec4(sin(normal.xy), cos(normal.xy));
             normal.xz  *= normal.w;
        return normal.xyz;
    }

    vec4 EncodeRGBE8(vec3 rgb) {
        const float a = 128.0 / 255.0;
        const float b = 1.0 / 255.0;
        float exponent = floor(log2(max(max(rgb.r, rgb.g), rgb.b)));
        vec3  mantissa = saturate(a * rgb / exp2(exponent));
              exponent = saturate((exponent + 127.0) * b);
        return vec4(mantissa, exponent);
    }

    vec3 DecodeRGBE8(vec4 rgbe) {
        const float a = 510 / 256.0;
        float exponent = exp2(rgbe.a * 255.0 - 127.0);
        vec3  mantissa = a * rgbe.rgb;
        return exponent * mantissa;
    }

    const float dynamicRange    = 20000.0;
    const float dynamicRangeRCP = rcp(dynamicRange);

    #define EncodeColour(x) (x * dynamicRangeRCP)
    #define DecodeColour(x) (x * dynamicRange)

#endif
