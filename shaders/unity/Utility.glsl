/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_UTILITY
    #define UNITY_INCL_UTILITY

    #define DEFINE_genFType(func) func(float) func(vec2) func(vec3) func(vec4)
    #define DEFINE_genVType(func) func(vec2) func(vec3) func(vec4)
    #define DEFINE_genDType(func) func(double) func(dvec2) func(dvec3) func(dvec4)
    #define DEFINE_genIType(func) func(int) func(ivec2) func(ivec3) func(ivec4)
    #define DEFINE_genUType(func) func(uint) func(uvec2) func(uvec3) func(uvec4)
    #define DEFINE_genBType(func) func(bool) func(bvec2) func(bvec3) func(bvec4)

    #include "/unity/utility/Compat.glsl"

    #include "/unity/utility/TypeAuto.glsl"
    #include "/unity/utility/TypeFloat.glsl"
    #include "/unity/utility/TypeMatrix.glsl"
    #include "/unity/utility/TypeVector.glsl"

    #include "/unity/utility/Constant.glsl"

    #include "/unity/utility/Encoding.glsl"
    #include "/unity/utility/Dither.glsl"
    #include "/unity/utility/Color.glsl"
    #include "/unity/utility/PointMapping.glsl"
    #include "/unity/utility/Scene.glsl"
    #include "/unity/utility/Volume.glsl"
    #include "/unity/utility/SphericalHarmonics.glsl"

#endif
