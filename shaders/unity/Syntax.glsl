/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_SYNTAX
    #define UNITY_INCL_SYNTAX

    const float PI  = radians(180.0);
    const float rPI = 1.0 / PI;
    const float hPI = 0.5 * PI;

    const float TAU  = radians(360.0);
    const float rTAU = 1.0 / TAU;

    const float PHI  = sqrt(5.0) * 0.5 + 0.5;
    const float rPHI = 1.0 / PHI;

    const float GOLDEN  = TAU / PHI / PHI;
    const float rGOLDEN = 1.0 / GOLDEN;

    const float UBYTE  = exp2(8);
    const float rUBYTE = 1.0 / UBYTE;

    const float UHALF  = exp2(16);
    const float rUHALF = 1.0 / UHALF;

    const float UINT  = exp2(32);
    const float rUINT = 1.0 / UINT;

    const float ULONG  = exp2(64);
    const float rULONG = 1.0 / ULONG;

    const float LOG2  = log(2.0);
    const float rLOG2 = 1.0 / LOG2;

    #include "/local/Settings.glsl"

    #define TIME ( frameTimeCounter * GLOBAL_TIME_MULT )

#endif
