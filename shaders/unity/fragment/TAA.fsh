/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_FRAG_TAA
    #define UNITY_INCL_FRAG_TAA

    #include "/unity/shared/SpaceConversion.glsl"

    vec2 CalculateVelocity(const vec2 screenCoord, const float depth) {
        vec3 velocity = ViewToScenePosition(ScreenToViewPosition(screenCoord, depth));
             velocity = (cameraPosition - previousCameraPosition) + velocity;
             velocity = transMAD(gbufferPreviousModelView, velocity);
             velocity = (diagonal3(gbufferPreviousProjection) * velocity + gbufferPreviousProjection[3].xyz) / -velocity.z * 0.5 + 0.5;

        return screenCoord - velocity.xy;
    }

    vec3 FindClosestFragment3x3(const vec2 screenCoord) {
        #define depthmap depthtex1

        vec3 depth1 = vec3(pixelSize.x, 0.0, texture2D(depthmap, screenCoord + vec2(pixelSize.x, 0.0)).x);
        vec3 depth2 = vec3(-pixelSize.x, 0.0, texture2D(depthmap, screenCoord + vec2(-pixelSize.x, 0.0)).x);
        vec3 depth3 = vec3(pixelSize.x, pixelSize.y, texture2D(depthmap, screenCoord + vec2(pixelSize.x, pixelSize.y)).x);
        vec3 depth4 = vec3(-pixelSize.x, pixelSize.y, texture2D(depthmap, screenCoord + vec2(-pixelSize.x, pixelSize.y)).x);
        vec3 depth5 = vec3(pixelSize.x, -pixelSize.y, texture2D(depthmap, screenCoord + vec2(pixelSize.x, -pixelSize.y)).x);
        vec3 depth6 = vec3(-pixelSize.x, -pixelSize.y, texture2D(depthmap, screenCoord + vec2(-pixelSize.x, -pixelSize.y)).x);
        vec3 depth7 = vec3(0.0, pixelSize.y, texture2D(depthmap, screenCoord + vec2(0.0, pixelSize.y)).x);
        vec3 depth8 = vec3(0.0, -pixelSize.y, texture2D(depthmap, screenCoord + vec2(0.0, -pixelSize.y)).x);

        #undef depthmap

        vec3 depthMin = depth1;
        depthMin = depthMin.z > depth2.z ? depth2 : depthMin;
        depthMin = depthMin.z > depth3.z ? depth3 : depthMin;

        depthMin = depthMin.z > depth4.z ? depth4 : depthMin;
        depthMin = depthMin.z > depth5.z ? depth5 : depthMin;
        depthMin = depthMin.z > depth6.z ? depth6 : depthMin;

        depthMin = depthMin.z > depth7.z ? depth7 : depthMin;
        depthMin = depthMin.z > depth8.z ? depth8 : depthMin;

        return vec3(depthMin.xy + screenCoord, depthMin.z);
    }

    vec3 ClipAABB(const vec3 aabbMin, const vec3 aabbMax, const vec3 q) {
        // note: only clips towards aabb center (but fast!)
        vec3 p_clip = 0.5 * (aabbMax + aabbMin);
        vec3 e_clip = 0.5 * (aabbMax - aabbMin) + 1e-8;

        vec3 v_clip = q - p_clip;
        vec3 v_unit = v_clip.xyz / e_clip;
        vec3 a_unit = abs(v_unit);
        float ma_unit = max(a_unit.x, max(a_unit.y, a_unit.z));

        if (ma_unit > 1.0)
            return p_clip + v_clip / ma_unit;
        else
            return q;// point inside aabb
    }

    vec3 SampleCurrentFrame(const vec2 screenCoord) {
        return DecodeRGBE8(texture2D(colortex4, screenCoord));
    }

    vec3 CalculateReprojection(vec2 screenCoord, const vec2 velocity) {
        const float a = 1.0 / 9.0;

        vec3 currentSample = SampleCurrentFrame(screenCoord);

        vec3 sample1 = SampleCurrentFrame(screenCoord + vec2( pixelSize.x, 0.0));
        vec3 sample2 = SampleCurrentFrame(screenCoord + vec2(-pixelSize.x, 0.0));
        vec3 sample3 = SampleCurrentFrame(screenCoord + vec2( pixelSize.x,  pixelSize.y));
        vec3 sample4 = SampleCurrentFrame(screenCoord + vec2(-pixelSize.x,  pixelSize.y));
        vec3 sample5 = SampleCurrentFrame(screenCoord + vec2( pixelSize.x, -pixelSize.y));
        vec3 sample6 = SampleCurrentFrame(screenCoord + vec2(-pixelSize.x, -pixelSize.y));
        vec3 sample7 = SampleCurrentFrame(screenCoord + vec2(0.0,  pixelSize.y));
        vec3 sample8 = SampleCurrentFrame(screenCoord + vec2(0.0, -pixelSize.y));

        vec3 sampleMin = min(currentSample, min(min4f(sample1, sample2, sample3, sample4), min4f(sample5, sample6, sample7, sample8)));
        vec3 sampleMax = max(currentSample, max(max4f(sample1, sample2, sample3, sample4), max4f(sample5, sample6, sample7, sample8)));
        vec3 sampleAvg = (currentSample + sample1 + sample2 + sample3 + sample4 + sample5 + sample6 + sample7 + sample8) * a;

        vec3 previousSample = ClipAABB(sampleMin, sampleMax, DecodeColour(texture2D(colortex3, screenCoord - velocity).rgb));

        vec2 pixelVelocity = abs(fract(velocity * viewDimensions) - 0.5) * 2.0;

        float blendWeight = 0.97 * (sqrt(pixelVelocity.x * pixelVelocity.y) * 0.125 + 0.875);

        screenCoord -= velocity;
        blendWeight  = saturate(screenCoord) != screenCoord ? 0.0 : blendWeight;
        
        #define sharpening (1.0 - exp(-(currentSample - clamp(sampleAvg, sampleMin, sampleMax)))) * TAA_SHARPENING

        return mix(currentSample + sharpening, previousSample, blendWeight);

        #undef sharpening
    }

    vec3 CalculateTAA(const vec2 screenCoord) {
    #ifndef TAA
        return SampleCurrentFrame(screenCoord);
    #endif

        float backDepth = texture2D(depthtex1, screenCoord).x;
        float handDepth = texture2D(depthtex2, screenCoord).x;

        vec3 closest = FindClosestFragment3x3(screenCoord);

        return CalculateReprojection(screenCoord, (handDepth > backDepth) ? vec2(0.0) : CalculateVelocity(closest.xy, closest.z));
    }

#endif
