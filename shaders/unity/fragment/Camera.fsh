/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_FRAG_CAMERA
    #define UNITY_INCL_FRAG_CAMERA

    float CalculateEV100(const float aperture2, const float shutterSpeed, const float ISO) {
        return log2(aperture2 / shutterSpeed * 100.0 / ISO);
    }

    float CalculateEV100Auto(const float avg) {
        return log2(avg * 8.0);
    }

    float EV100ToExposure(const float EV100) {
        return (1.0 / 1.2) * exp2(-EV100);
    }

    float CalculateExposure(const float avg) {
        const float aperture = CAMERA_APERTURE;
        const float aperture2 = aperture * aperture;
        const float shutterSpeed = 1.0 / CAMERA_SHUTTER_SPEED;
        const float exposureOffset = CAMERA_EV_OFFSET;
        const float ISO = CAMERA_ISO;

    #if CAMERA_MODE == CAMERA_MODE_MANUAL
        float exposureValue = CalculateEV100(aperture2, shutterSpeed, ISO);
    #else
        float exposureValue = CalculateEV100Auto(clamp(avg, 1.0, 8192.0));
    #endif

        return EV100ToExposure(exposureValue - exposureOffset);
    }

#endif
