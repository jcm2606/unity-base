/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_SHARED_PHYSICAL_SKY
    #define UNITY_INCL_SHARED_PHYSICAL_SKY

	#include "/unity/shared/Noise.glsl"

    /*******************************************************************************
	- Phases
	******************************************************************************/

	#define phaseg0 0.25

	float sky_rayleighPhase(const float cosTheta) {
		const float atten = 3.0 / (16.0 * PI);
		return (cosTheta * cosTheta) * atten + atten;
	}

	float sky_miePhase(const float cosTheta, const float g) {
		const_arg float gg = g * g;
		const float PI4 = PI * 4.0;
		return (1.0 - gg) / (PI4 * pow(1.0 + gg - 2.0 * g * cosTheta, 1.5));
	}

	vec2 sky_phase(const float cosTheta, const float g) {
		return vec2(sky_rayleighPhase(cosTheta), sky_miePhase(cosTheta, g));
	}

	/*******************************************************************************
	- Sky Functions
	******************************************************************************/

	float CalculateStars(vec3 sceneDirection) {
		const float res = 2048.0 * 0.15;
		const float starCoverage = 0.001;

		sceneDirection = sceneDirection * res * RotateMatrix(vec3(0.0, 1.0, 0.0), sunDirection);

		vec3 id  = floor(sceneDirection);
		vec3 q   = id - sceneDirection + 0.5;
		vec3 rn  = hash33(id);

		return ((1.0 - smoothstep(0.0, 0.5, flength(q))) * step(rn.x, starCoverage)) * 16.0;
	}

	float CalculateSunSpot(const float VdotL) {
		const float cosSunRadius = cos(sunAngularDiameter);
		const float sunLuminance = 1.0 / ((1.0 - cosSunRadius) * PI);

		return fstep(cosSunRadius, VdotL) * sunLuminance;
	}

	float CalculateMoonSpot(const float VdotL) {
		const float cosMoonRadius = cos(moonAngularDiameter);
		const float moonLuminance = 1.0 / ((1.0 - cosMoonRadius) * PI);

		return fstep(cosMoonRadius, VdotL) * moonLuminance;
	}

	/*******************************************************************************
	- Sky Functions
	******************************************************************************/

	// No intersection if returned y component is < 0.0
	vec2 rsi(const vec3 position, const vec3 direction, const float radius) {
		float PoD = dot(position, direction);
		const_arg float radiusSquared = radius * radius;

		float delta = PoD * PoD + radiusSquared - dot(position, position);
		if (delta < 0.0) return vec2(-1.0);
			delta = sqrt(delta);

		return -PoD + vec2(-delta, delta);
	}

	vec3 sky_atmosphereDensity(const float centerDistance) {
		const_arg vec2 rayleighMie = exp(centerDistance * -atmosphereInverseScaleHeights + atmosphereScaledPlanetRadius);

		const_arg float ozone = exp(-max(0.0, (35000.0 - centerDistance) - atmospherePlanetRadius) * 0.0002)
					* exp(-max(0.0, (centerDistance - 35000.0) - atmospherePlanetRadius) / 15000.0);

		return vec3(rayleighMie, ozone);
	}

	// I don't know if "thickness" is the right word, using it because Jodie uses it for that and I can't think of (or find) anything better.
	vec3 sky_atmosphereThickness(vec3 position, const vec3 direction, const float rayLength, const float steps) {
		const_arg float stepSize  = rayLength / steps;
		const_arg vec3  increment = direction * stepSize;
		position += increment * 0.5;

		vec3 thickness = vec3(0.0);
		for (float i = 0.0; i < steps; ++i, position += increment) {
			thickness += sky_atmosphereDensity(length(position));
		}

		return thickness * stepSize;
	}
	vec3 sky_atmosphereThickness(const vec3 position, const vec3 direction, const float steps) {
		float rayLength = dot(position, direction);
			  rayLength = sqrt(rayLength * rayLength + atmosphereRadiusSquared - dot(position, position)) - rayLength;

		return sky_atmosphereThickness(position, direction, rayLength, steps);
	}

	vec3 sky_atmosphereOpticalDepth(const vec3 position, const vec3 direction, const float rayLength, const float steps) {
		return atmosphereAttenuationCoefficients * sky_atmosphereThickness(position, direction, rayLength, steps);
	}
	vec3 sky_atmosphereOpticalDepth(const vec3 position, const vec3 direction, const float steps) {
		return atmosphereAttenuationCoefficients * sky_atmosphereThickness(position, direction, steps);
	}

	vec3 sky_atmosphereTransmittance(const vec3 position, const vec3 direction, const float steps) {
		return exp2(-sky_atmosphereOpticalDepth(position, direction, steps) * rLOG2);
	}
	
	vec3 GetSunColorZom(const vec3 lightDirection) {
		return sky_phase(1.0, atmosphereMieG).x * sky_phase(1.0, atmosphereMieG).y * sky_atmosphereTransmittance(vec3(0.0, atmospherePlanetRadius, 0.0), lightDirection, 8) * skySunColor;
	}

	vec3 GetMoonColorZom(const vec3 lightDirection) {
		return sky_atmosphereTransmittance(vec3(0.0, atmospherePlanetRadius, 0.0), lightDirection, 8) * skyMoonColor;
	}
	
	vec3 sky_atmosphere(const vec3 background, const vec3 viewVector, const vec3 upVector, const vec3 sunVector, const vec3 moonVector, const vec3 sunIlluminance, const vec3 moonIlluminance, const int iSteps, inout vec3 transmittance) {
		const int jSteps = 3;  // For very high quality: 10 is good, can probably get away with less

        const_arg float iSteps_r = 1.0 / iSteps;

		vec3 viewPosition = (atmospherePlanetRadius + eyeAltitude) * upVector;

		vec2 aid = rsi(viewPosition, viewVector, atmosphereRadius);
		if (aid.y < 0.0) return background;
		vec2 pid = rsi(viewPosition, viewVector, atmospherePlanetRadius * 0.998);

		bool pi = pid.y >= 0.0;

		vec2 sd = vec2((pi && pid.x < 0.0) ? pid.y : max(aid.x, 0.0), (pi && pid.x > 0.0) ? pid.x : aid.y);

		float stepSize  = (sd.y - sd.x) * iSteps_r;
		vec3  increment = viewVector * stepSize;
		vec3  position  = viewVector * sd.x + (increment * 0.3 + viewPosition);

		vec2 phaseSun  = sky_phase(dot(viewVector, sunVector ), atmosphereMieG);
		vec2 phaseMoon = sky_phase(dot(viewVector, moonVector), atmosphereMieG);

		vec3 scatteringSun  = vec3(0.0);
		vec3 scatteringMoon = vec3(0.0);
		vec3 scatteringAmbient = vec3(0.0);
			transmittance = vec3(1.0);

		for (int i = 0; i < iSteps; ++i, position += increment) {
			vec3 density          = sky_atmosphereDensity(length(position));
			if (density.y > 1e35) break;
			vec3 stepAirmass      = density * stepSize;
			vec3 stepOpticalDepth = atmosphereAttenuationCoefficients * stepAirmass;

			vec3 stepTransmittance       = exp2(-stepOpticalDepth * rLOG2);
			vec3 stepTransmittedFraction = clamp((stepTransmittance - 1.0) / -stepOpticalDepth, 0.0, 1.0);
			vec3 stepScatteringVisible   = transmittance * stepTransmittedFraction;

			scatteringSun  += (atmosphereScatteringCoefficients * (stepAirmass.xy * phaseSun )) * stepScatteringVisible * sky_atmosphereTransmittance(position, sunVector,  jSteps);
			scatteringMoon += (atmosphereScatteringCoefficients * (stepAirmass.xy * phaseMoon)) * stepScatteringVisible * sky_atmosphereTransmittance(position, moonVector, jSteps);

			transmittance  *= stepTransmittance;
		}

		vec3 scattering = scatteringSun * sunIlluminance + scatteringMoon * moonIlluminance;

		return (!pi ? background * transmittance : vec3(0.0)) + mix(scattering, vec3(dot(scattering, vec3(0.2125, 0.7154, 0.0721))), wetness);
	}

#endif
