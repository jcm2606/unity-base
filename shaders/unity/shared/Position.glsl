/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_SHARED_POSITION
    #define UNITY_INCL_SHARED_POSITION

    #include "/unity/shared/SpaceConversion.glsl"

    struct Position {
        float depth;

        vec3 viewPosition;
        vec3 scenePosition;
    };

    vec3 GetViewPosition(vec2 screenCoord, sampler2D depthtex) {
        return ScreenToViewPosition(screenCoord, texture2D(depthtex, screenCoord).x);
    }

    vec3 GetScenePosition(vec2 screenCoord, sampler2D depthtex) {
        return ViewToScenePosition(ScreenToViewPosition(screenCoord, texture2D(depthtex, screenCoord).x));
    }

    Position GetPosition(vec2 screenCoord, sampler2D depthtex) {
        Position position;
        position.depth = texture2D(depthtex, screenCoord).x;
    #ifdef TAA
        screenCoord -= taaJitter * 0.5;
    #endif
        position.viewPosition = ScreenToViewPosition(screenCoord, position.depth);
        position.scenePosition = ViewToScenePosition(position.viewPosition);

        return position;
    }

#endif
