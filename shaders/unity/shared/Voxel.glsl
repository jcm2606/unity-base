/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_SHARED_VOXEL
    #define UNITY_INCL_SHARED_VOXEL

    const int shadowMapResolution = 4096; // [1024 2048 4096 8192 16384]

    bool IsInVoxelizationVolume(ivec3 voxelIndex) {
        const int xzRadiusBlocks = shadowMapResolution / 32;
        const ivec3 lo = ivec3(-xzRadiusBlocks    ,   0,-xzRadiusBlocks    );
        const ivec3 hi = ivec3( xzRadiusBlocks - 1, 255, xzRadiusBlocks - 1);

        return clamp(voxelIndex, lo, hi) == voxelIndex;
    }

    vec3 SceneSpaceToVoxelSpace(vec3 scenePosition) {
        scenePosition.xz += fract(cameraPosition.xz);
        scenePosition.y  += cameraPosition.y;
        return scenePosition;
    }
    vec3 WorldSpaceToVoxelSpace(vec3 worldPosition) {
        worldPosition.xz -= floor(cameraPosition.xz);
        return worldPosition;
    }

    ivec2 GetVoxelStoragePos(ivec3 voxelIndex) { // in pixels/texels
        return ((voxelIndex.xz + (shadowMapResolution / 32)) * 16 + ivec2(voxelIndex.y % 16, voxelIndex.y / 16)) / 2;
    }

    #if !defined shadow
        vec4[2] ReadVoxel(ivec3 voxelPosition) {
            ivec2 storagePos = GetVoxelStoragePos(voxelPosition);

            return vec4[2](
                texelFetch(shadowcolor0, storagePos, 0),
                texelFetch(shadowcolor1, storagePos, 0)
            );
        }
    #endif
    /*
    void SetVoxelTint(inout vec4[2] voxel, vec3 tint) {
        voxel[0].rgb = tint;
    }
    void SetVoxelId(inout vec4[2] voxel, int id) {
        voxel[0].a = 1.0 - id / 255.0;
    }
    void SetVoxelTileSize(inout vec4[2] voxel, int tileSize) {
        voxel[1].y = Pack2x4(vec2(Unpack2x4(voxel[1].y).x, saturate(log2(float(tileSize)) / 15.0)));
    }
    void SetVoxelTileIndex(inout vec4[2] voxel, ivec2 tileIndex) {
        voxel[1].zw = (tileIndex) / 255.0;
    }

    vec3 ExtractVoxelTint(vec4[2] voxel) {
        return voxel[0].rgb;
    }
    int ExtractVoxelId(vec4[2] voxel) {
        return int(floor(255.5 - 255.0 * voxel[0].a));
    }
    int ExtractVoxelTileSize(vec4[2] voxel) {
        return int(exp2(floor(Unpack2x4(voxel[1].y).y * 15.0 + 0.5)));
    }
    ivec2 ExtractVoxelTileIndex(vec4[2] voxel) {
        return ivec2(floor(voxel[1].zw * 255.0 + 0.5));
    }
    */
#endif
