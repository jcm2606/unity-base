/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_SHARED_NOISE
    #define UNITY_INCL_SHARED_NOISE

    #define noise2D(coord) texture2D(noisetex, fract(coord)).xy

    vec2 noise2DSmooth(vec2 coord) {
        const float resolution    = 64.0;
        const float resolutionRCP = rcp(resolution);

        coord *= resolution;
        coord += 0.5;

        vec2 whole = floor(coord);
        vec2 part  = fract(coord);

        part.x = part.x * part.x * (3.0 - 2.0 * part.x);
        part.y = part.y * part.y * (3.0 - 2.0 * part.y);

        coord = whole + part;

        coord -= 0.5;
        coord *= resolutionRCP;

        return texture2D(noisetex, fract(coord)).xy;
    }

    vec3 hash33(vec3 p) {
        p = fract(p * vec3(443.8975, 397.2973, 491.1871));
        p += dot(p.zxy, p.yxz + 19.27);
        return fract(vec3(p.x * p.y, p.z * p.x, p.y * p.z));
    }

#endif
