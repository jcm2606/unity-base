/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_SHARED_SHADOW_CONVERSION
    #define UNITY_INCL_SHARED_SHADOW_CONVERSION

    vec3 SceneToShadowPosition(vec3 scenePosition) {
        vec3 shadowPosition    = transMAD(shadowModelView, scenePosition);
             shadowPosition    = projMAD3(shadowProjection, shadowPosition);
             shadowPosition.z *= shadowDepthMult;

        return shadowPosition;
    }

    float ShadowDistortionOperator(vec2 shadowPosition) {
        return flength(shadowPosition * 1.165) * SHADOW_DISTORTION_FACTOR + (1.0 - SHADOW_DISTORTION_FACTOR);
    }

    float CalculateShadowDistortionMult(vec2 shadowPosition) {
        return 1.0 / ShadowDistortionOperator(shadowPosition);
    }

    vec2 DistortShadowPosition(vec2 shadowPosition) {
        return shadowPosition * CalculateShadowDistortionMult(shadowPosition);
    }

    vec2 DistortShadowPositionProj(vec2 shadowPosition) {
        vec2 distorted = shadowPosition * 2.0 - 1.0;
        distorted = DistortShadowPosition(distorted);
        return distorted * 0.5 + 0.5;
    }

    vec3 DistortShadowPosition(vec3 shadowPosition) {
        return vec3(DistortShadowPosition(shadowPosition.xy), shadowPosition.z);
    }

    float CalculateShadowDistortionFactor(vec2 shadowPosition) {
        return flength(shadowPosition) * ShadowDistortionOperator(shadowPosition) + 1.0;
    }

#endif
