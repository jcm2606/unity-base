/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_SHARED_SPACE_CONVERSION
    #define UNITY_INCL_SHARED_SPACE_CONVERSION

    vec3 ScreenToViewPosition(vec2 screenCoord, float depth) {
        vec3 screenPosition = vec3(screenCoord, depth) * 2.0 - 1.0;

        return projMAD3(gbufferProjectionInverse, screenPosition) / (screenPosition.z * gbufferProjectionInverse[2].w + gbufferProjectionInverse[3].w);
    }

    vec3 ViewToScreenPosition(vec3 viewPosition) {
        return (projMAD3(gbufferProjection, viewPosition) / -viewPosition.z) * 0.5 + 0.5;
    }

    vec3 ViewToScenePosition(vec3 viewPosition) {
        return transMAD(gbufferModelViewInverse, viewPosition);
    }

    vec3 SceneToViewPosition(vec3 scenePosition) {
        return transMAD(gbufferModelView, scenePosition);
    }

    vec4 ProjectViewPosition(vec3 viewPosition) {
        return vec4(projMAD3(gbufferProjection, viewPosition), viewPosition.z * gbufferProjection[2].w);
    }
    
    float ViewToScreenDepth(float viewDepth) {
        return ((gbufferProjection[2].z * viewDepth + gbufferProjection[3].z) / -viewDepth) * 0.5 + 0.5;
    }

    float ScreenToViewDepth(float screenDepth) {
        screenDepth = screenDepth * 2.0 - 1.0;
        return gbufferProjectionInverse[3].z / (gbufferProjectionInverse[2].w * screenDepth + gbufferProjectionInverse[3].w);
    }

#endif
