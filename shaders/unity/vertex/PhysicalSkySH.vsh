/*
    SHADER_NAME, by AUTHOR_NAME
    Using the Unity Shader Base, by jcm2606
*/

#if !defined UNITY_INCL_VERT_PHYSICAL_SKY_SH
    #define UNITY_INCL_VERT_PHYSICAL_SKY_SH

    #include "/unity/shared/PhysicalSky.glsl"

	void CalculateSkySH(vec3 viewAbsorb, vec3 skyColour) {
        const int latSamples = 5;
        const int lonSamples = 5;
        const float rLatSamples = 1.0 / latSamples;
        const float rLonSamples = 1.0 / lonSamples;
        const float sampleCount = rLatSamples * rLonSamples;

        const float latSize = rLatSamples * PI;
        const float lonSize = rLonSamples * TAU;

        vec4 shR = vec4(0.0), shG = vec4(0.0), shB = vec4(0.0);
        const float offset = 0.1;

        const int skySamples = 10;

        for(int i = 0; i < latSamples; ++i) {
            float latitude = float(i) * latSize;

            for(int j = 0; j < lonSamples; ++j) {
                float longitude = float(j) * lonSize;

                float c = cos(latitude);
                vec3 kernel = vec3(c * cos(longitude), sin(latitude), c * sin(longitude));

                vec3 skyCol = sky_atmosphere(vec3(0.0), normalize(kernel + vec3(0.0, offset, 0.0)), vec3(0.0, 1.0, 0.0), sunDirection, -sunDirection, skySunColor, skyMoonColor, skySamples, viewAbsorb);

                shR += ToSH(skyCol.r, kernel);
                shG += ToSH(skyCol.g, kernel);
                shB += ToSH(skyCol.b, kernel);
            }
        }

        skySH = mat3x4(shR, shG, shB) * TAU * sampleCount;
    }

#endif
